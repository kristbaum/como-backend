import csv


def split_csv(filename, lines_per_file=10000):

    with open(filename, "r", newline="") as file:
        reader = csv.reader(file)
        header = next(reader)  # Read the header row

        file_count = 0
        while True:
            output_file = f"{filename}_part{file_count}.csv"
            with open(output_file, "w", newline="") as outfile:
                writer = csv.writer(outfile)
                writer.writerow(header)  # Write the header in each new file

                for i, row in enumerate(reader):
                    writer.writerow(row)
                    if i + 1 == lines_per_file - 1:  # Check if current file is filled
                        break

            try:
                next_line = next(reader)
            except StopIteration:
                break  # Exit the loop if no more lines to read

            # If not done, start a new file and add the next line to it
            file_count += 1
            with open(f"{filename}_part{file_count}.csv", "w", newline="") as outfile:
                writer = csv.writer(outfile)
                writer.writerow(header)
                writer.writerow(next_line)  # Write the first line of the new file

            file_count += 1


if __name__ == "__main__":
    split_csv("upload-goolie-test.csv")

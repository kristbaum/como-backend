import json
import csv


def flatten_jsonl_to_csv(input_jsonl_path, output_csv_path):
    with open(input_jsonl_path, "r", encoding="utf-8") as jsonl_file, open(
        output_csv_path, "w", newline="", encoding="utf-8"
    ) as csv_file:

        csv_writer = csv.writer(csv_file)
        # Write the header for the CSV file
        csv_writer.writerow(
            [
                "id",
                "document_index",
                "entry_index",
                "original_file",
                "text",
                "language",
                "triplet_type",
                "subject_label",
                "relation_label",
                "relation_wikidata_id",
                "object_label",
                "object_wikidata_id",
            ]
        )

        for line in jsonl_file:
            entry = json.loads(line)
            base_fields = [
                entry["id"],
                entry["document_index"],
                entry["entry_index"],
                entry["original_file"],
                entry["text"],
                entry["language"],
            ]

            # Handle the triplets
            if "triplets" in entry:
                for triplet in entry["triplets"]:
                    triplet_type = triplet["type"]
                    for content in triplet["content"]:
                        subject_label = content["subject"]["label"]
                        relation_label = content["relation"]["label"]
                        relation_wikidata_id = content["relation"].get(
                            "wikidata_id", ""
                        )
                        object_label = content["object"]["label"]
                        object_wikidata_id = content["object"].get("wikidata_id", "")

                        # Write the row with the triplet information
                        csv_writer.writerow(
                            base_fields
                            + [
                                triplet_type,
                                subject_label,
                                relation_label,
                                relation_wikidata_id,
                                object_label,
                                object_wikidata_id,
                            ]
                        )
            else:
                csv_writer.writerow(
                    base_fields + [None] * 6
                )  


flatten_jsonl_to_csv("gollie_filtered_merged.jsonl", "output.csv")

This is an early version of this process, with a lot of missing steps, consider using another version

```
SELECT ?file ?title ?image ?label 
WITH
{
  SELECT ?file ?title
  WHERE
  {
    SERVICE wikibase:mwapi
    {
      bd:serviceParam wikibase:api "Generator" .
      bd:serviceParam wikibase:endpoint "commons.wikimedia.org" .
      bd:serviceParam mwapi:gcmtitle "Category:Details_of_paintings_by_Rubens" .
      bd:serviceParam mwapi:generator "categorymembers" .
      bd:serviceParam mwapi:gcmtype "file" .
      bd:serviceParam mwapi:gcmlimit "max" .
      ?title wikibase:apiOutput mwapi:title .
      ?pageid wikibase:apiOutput "@pageid" .
    }
    BIND (URI(CONCAT('https://commons.wikimedia.org/entity/M', ?pageid)) AS ?file)
  }
} AS %get_files
WHERE
{
  INCLUDE %get_files

  ?file schema:url ?image.

  # Check for absence of English captions
  MINUS {
    SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
    ?file rdfs:label ?label.
    FILTER(LANG(?label) = "en")
  }
}
```

Describe the image by following the style of this description:
Close-up of a young woman with a gentle smile, flanked by two older men, one looking down and the other to the side from Rubens „The Drunken Silenus“. 
Use the information on this website https://commons.wikimedia.org/entity/M158597
import csv
import json

# Paths to input files
input_csv_path = "query_with_thumbs.csv"
jsonl_path = "results_gpt-4o.jsonl"
output_csv_path = "query_with_descriptions.csv"

# Read JSONL results
results = {}
with open(jsonl_path, mode="r", newline="", encoding="utf-8") as jsonl_file:
    for line in jsonl_file:
        result = json.loads(line)
        custom_id = result["custom_id"]
        description = result["response"]["body"]["choices"][0]["message"]["content"]
        results[custom_id] = description

# Read the input CSV file and add descriptions
with open(input_csv_path, mode="r", newline="", encoding="utf-8") as infile:
    csv_reader = csv.DictReader(infile)
    fieldnames = csv_reader.fieldnames + ["description"]

    rows = []
    for i, row in enumerate(csv_reader, start=1):
        custom_id = f"request-{i}"
        row["description"] = results.get(custom_id, "No description available")
        rows.append(row)

# Write the updated rows to a new CSV file
with open(output_csv_path, mode="w", newline="", encoding="utf-8") as outfile:
    csv_writer = csv.DictWriter(outfile, fieldnames=fieldnames)
    csv_writer.writeheader()
    csv_writer.writerows(rows)

print(f"Updated CSV with descriptions has been written to {output_csv_path}.")

import csv
import requests
import json

headers = {"User-Agent": "Filelink converter"}

base_url = "https://api.wikimedia.org/core/v1/commons/file/"

input_csv_path = "query_full.csv"
output_csv_path = "query_with_thumbs.csv"

with open(input_csv_path, mode="r", newline="", encoding="utf-8") as file:
    csv_reader = csv.DictReader(file)
    rows = list(csv_reader)

fieldnames = list(rows[0].keys()) if rows else []
fieldnames.append("thumburl")

with open(output_csv_path, mode="w", newline="", encoding="utf-8") as file:
    csv_writer = csv.DictWriter(file, fieldnames=fieldnames)
    csv_writer.writeheader()

    for row in rows:
        file_name = row["title"]
        url = base_url + file_name

        response = requests.get(url, headers=headers)
        data = json.loads(response.text)

        print(data)

        if "url" in data["preferred"]:
            thumb_url = data["preferred"]["url"]
        else:
            thumb_url = "No thumbnail available"

        row["thumburl"] = thumb_url

        csv_writer.writerow(row)

print("Updated CSV has been saved.")

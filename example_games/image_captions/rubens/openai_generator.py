import openai
import csv

client = openai.OpenAI(
    api_key=""
)


input_csv_path = "query_with_thumbs.csv"
output_csv_path = "query_with_descriptions.csv"


with open(input_csv_path, mode="r", newline="", encoding="utf-8") as infile:
    csv_reader = csv.DictReader(infile)
    rows = list(csv_reader)

with open(output_csv_path, mode="w", newline="", encoding="utf-8") as outfile:
    fieldnames = csv_reader.fieldnames + ["new_description"]
    csv_writer = csv.DictWriter(outfile, fieldnames=fieldnames)
    csv_writer.writeheader()

    for row in rows:
        image_url = row["thumburl"]
        response = client.chat.completions.create(
            model="gpt-4-turbo",
            messages=[
                {
                    "role": "user",
                    "content": [
                        {
                            "type": "text",
                            "text": """Describe the image by following the style of this description:
Close-up of a young woman with a gentle smile, flanked by two older men, one looking down and the other to the side from Rubens „The Drunken Silenus“. 
Use the information on this website: """
                            + row["file"],
                        },
                        {
                            "type": "image_url",
                            "image_url": {
                                "url": image_url,
                            },
                        },
                    ],
                }
            ],
            max_tokens=200,
        )
        print(response.choices[0].message.content)
        new_description = response.choices[0].message.content
        row["new_description"] = new_description
        csv_writer.writerow(row)
        print(new_description)
        exit()

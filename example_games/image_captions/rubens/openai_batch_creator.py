import csv
import json

input_csv_path = "query_with_thumbs.csv"
output_jsonl_path = "requests.jsonl"

with open(input_csv_path, mode="r", newline="", encoding="utf-8") as infile:
    csv_reader = csv.DictReader(infile)
    rows = list(csv_reader)

with open(output_jsonl_path, mode="w", newline="", encoding="utf-8") as outfile:
    request_id = 1  # To generate a unique custom_id for each request
    for row in rows:
        image_url = row["thumburl"]
        request_body = {
            "model": "gpt-4o",
            "messages": [
                {
                    "role": "system",
                    "content": [
                        {
                            "type": "text",
                            "text": 'You are a "short, one-line description" writer for Wikimedia Commons',
                        }
                    ],
                },
                {
                    "role": "user",
                    "content": [
                        {
                            "type": "image_url",
                            "image_url": {"url": image_url},
                        },
                        {
                            "type": "text",
                            "text": f'A detail of a Rubens Painting, with the filename "{row["file"]}"',
                        },
                    ],
                },
            ],
            "temperature": 1,
            "max_tokens": 256,
            "top_p": 1,
            "frequency_penalty": 0,
            "presence_penalty": 0,
        }

        jsonl_object = {
            "custom_id": f"request-{request_id}",
            "method": "POST",
            "url": "/v1/chat/completions",
            "body": request_body,
        }

        json.dump(jsonl_object, outfile)
        outfile.write("\n")
        request_id += 1

print(f"Requests have been written to {output_jsonl_path}.")

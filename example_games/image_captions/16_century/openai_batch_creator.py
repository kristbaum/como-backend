import csv
import json
import os

# Define paths
input_csv_path = "query_with_thumbs.csv"
output_dir = "output_chunks"
os.makedirs(output_dir, exist_ok=True)

# Estimated number of tokens per request (adjust this based on real data if necessary)
TOKENS_PER_REQUEST = 300  # Estimate based on messages and metadata
MAX_TOKENS_PER_FILE = 10000  # Maximum tokens per file

# Initialize variables
current_token_count = 0
file_index = 1
request_id = 1

# Initialize output file
output_jsonl_path = os.path.join(output_dir, f"requests_chunk_{file_index}.jsonl")
outfile = open(output_jsonl_path, mode="w", newline="", encoding="utf-8")

# Read the input CSV
with open(input_csv_path, mode="r", newline="", encoding="utf-8") as infile:
    csv_reader = csv.DictReader(infile)
    rows = list(csv_reader)

    # Write to the output JSONL file
    for row in rows:
        image_url = row["thumburl"]
        request_body = {
            "model": "gpt-4o",
            "messages": [
                {
                    "role": "system",
                    "content": [
                        {
                            "type": "text",
                            "text": "Write a concise one-line painting description for Wikimedia Commons.",
                        }
                    ],
                },
                {
                    "role": "user",
                    "content": [
                        {
                            "type": "image_url",
                            "image_url": {"url": image_url},
                        },
                        {
                            "type": "text",
                            "text": f'Filename: "{row["title"]}". Provide a one-line description, optionally using filename details.',
                        },
                    ],
                },
            ],
            "temperature": 1,
            "max_tokens": 256,
            "top_p": 1,
            "frequency_penalty": 0,
            "presence_penalty": 0,
        }

        jsonl_object = {
            "custom_id": f"request-{request_id}",
            "method": "POST",
            "url": "/v1/chat/completions",
            "body": request_body,
        }

        # Estimate tokens in this request
        tokens_in_request = TOKENS_PER_REQUEST

        # Check if adding this request exceeds the token limit for the current file
        if current_token_count + tokens_in_request > MAX_TOKENS_PER_FILE:
            # Close the current file and open a new one
            outfile.close()
            file_index += 1
            output_jsonl_path = os.path.join(
                output_dir, f"requests_chunk_{file_index}.jsonl"
            )
            outfile = open(output_jsonl_path, mode="w", newline="", encoding="utf-8")
            current_token_count = 0  # Reset token count for the new file

        # Write the current request to the file
        json.dump(jsonl_object, outfile)
        outfile.write("\n")
        current_token_count += tokens_in_request
        request_id += 1

# Close the last output file
outfile.close()

print(f"Requests have been split into chunks and saved in the directory: {output_dir}.")

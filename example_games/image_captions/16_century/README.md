```
SELECT ?file ?title ?image ?label 
WITH
{
  SELECT ?file ?title
  WHERE
  {
    SERVICE wikibase:mwapi
    {
      bd:serviceParam wikibase:api "Generator" .
      bd:serviceParam wikibase:endpoint "commons.wikimedia.org" .
      bd:serviceParam mwapi:gcmtitle "Category:1601-1650_paintings" .
      bd:serviceParam mwapi:generator "categorymembers" .
      bd:serviceParam mwapi:gcmtype "file" .
      bd:serviceParam mwapi:gcmlimit "max" .
      ?title wikibase:apiOutput mwapi:title .
      ?pageid wikibase:apiOutput "@pageid" .
    }
    BIND (URI(CONCAT('https://commons.wikimedia.org/entity/M', ?pageid)) AS ?file)
  }
} AS %get_files
WHERE
{
  INCLUDE %get_files

  ?file schema:url ?image.

  # Check for absence of English captions
  MINUS {
    SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
    ?file rdfs:label ?label.
    FILTER(LANG(?label) = "en")
  }
}
```

1. Run the query on https://commons-query.wikimedia.org and download as csv, and name it "query_initial.csv"
2. Run "convert_query_inital.py", which adds direct links to images in thumbnail size to the csv (takes a few seconds)
3. Then run "openai_batch_creator.py", which outputs "requests.jsonl" containing all of the batch to run on (in this case) OpenAIs platform
4. Put the resulting batches in a folder called: result_chunks
5. Run the script: "batch_result_to_csv"

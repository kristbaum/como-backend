# Name the column to be reconciled as 'recon_column'

# Add a new column with the following python code in OpenRefine:
import json

resultSet = []
for cand in cell.recon.candidates:
    resultSet.append(cand.id)
return json.dumps(resultSet)

# Add a new column per option
import json

id_list = json.loads(value)
column_value = id_list[0] if id_list else None

return column_value

# Now is a good time to add data from the reconciled columns to your dataset (that all will be included as part of the option in the Single Choice question)
# In this example the properties occuptation, birthdate and date of death are added to the dataset and integreated in a single column

import os
import csv
import json
import requests
from SPARQLWrapper import SPARQLWrapper, JSON
import urllib

# SPARQL endpoint and query to fetch items missing descriptions
ENDPOINT_URL = "https://query.wikidata.org/sparql"
QUERY = """SELECT ?item ?link WHERE {
  ?link schema:about ?item;
        schema:isPartOf <https://en.wikipedia.org/>.
  FILTER(NOT EXISTS {
    ?item schema:description ?desc.
    FILTER((LANG(?desc)) = "en")
  })
}
LIMIT 10
"""

CACHE_FILE = os.path.join("cache", "cached_data.jsonl")
GAME_TABLE_FILE = "game_data.csv"

# Ensure the cache directory exists
os.makedirs("cache", exist_ok=True)


# Function to append to cache file
def append_to_cache(data):
    with open(CACHE_FILE, "a") as f:
        f.write(json.dumps(data) + "\n")


# Function to load cached results
def load_cached_results():
    if os.path.exists(CACHE_FILE):
        with open(CACHE_FILE, "r") as f:
            return [json.loads(line) for line in f]
    return []


# Check if result is cached
def is_cached(title):
    cached = load_cached_results()
    for entry in cached:
        if entry.get("title") == title:
            return entry
    return None


# Fetch results from SPARQL endpoint
def get_sparql_results(endpoint_url, query):
    cached = load_cached_results()
    if any(entry.get("type") == "sparql_results" for entry in cached):
        print("Loaded cached SPARQL results.")
        return next(entry for entry in cached if entry.get("type") == "sparql_results")

    user_agent = "ComoGameCreator/1.0"
    sparql = SPARQLWrapper(endpoint_url, agent=user_agent)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    append_to_cache({"type": "sparql_results", "results": results["results"]})
    return results


# Fetch article description and first paragraph from Wikimedia API
def fetch_article_data(title):
    cached = is_cached(title)
    if cached:
        print(f"Loaded cached data for {title}.")
        return cached

    url = "https://api.wikimedia.org/service/lw/inference/v1/models/article-descriptions:predict"
    params = {"lang": "en", "title": title, "num_beams": 4, "debug": 1}
    headers = {"User-Agent": "Como/1.0"}

    try:
        response = requests.post(url, json=params, headers=headers)
        if response.status_code == 200:
            data = response.json()
            print(data)
            data["title"] = title
            append_to_cache(data)
            return data
        else:
            print(
                f"Error fetching data for {title}: {response.status_code} - {response.text}"
            )
    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")
    return None


# Save or update game table
def save_game_table(data):
    if not os.path.exists(GAME_TABLE_FILE):
        with open(GAME_TABLE_FILE, "w", newline="") as csvfile:
            writer = csv.writer(csvfile)
            headers = [
                "custom_id",
                "text1",
                "link1",
                "option1",
                "option2",
                "option3",
                "option4",
            ]
            writer.writerow(headers)
            writer.writerows(data)
    else:
        with open(GAME_TABLE_FILE, "a", newline="") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerows(data)


# Main workflow
if __name__ == "__main__":
    results = get_sparql_results(ENDPOINT_URL, QUERY)
    game_data = []
    print("Processing SPARQL results..." + str(results))

    for result in results["results"]["bindings"]:
        item_url = result["item"]["value"]
        link_url = result["link"]["value"]
        qid = item_url.split("/")[-1]

        print(f"Processing: {qid}")
        title = urllib.parse.unquote(link_url.split("/")[-1]).replace("_", " ")
        article_data = fetch_article_data(title)

        if article_data:
            predictions = article_data.get("prediction", ["No description available"])
            description = predictions[0] if predictions else "No description available"
            first_paragraph = (
                article_data.get("features", {})
                .get("first-paragraphs", {})
                .get("en", "No extract available")
            )
        else:
            continue

        link = f"https://www.wikidata.org/wiki/{qid}"

        # Ensure there are exactly 4 options for predictions
        options = predictions[:4] + ["" for _ in range(4 - len(predictions))]

        game_data.append([qid, first_paragraph, link, description] + options)

    save_game_table(game_data)
    print(f"Game table updated: {GAME_TABLE_FILE}")

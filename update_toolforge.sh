become como-backend
toolforge build start https://gitlab.wikimedia.org/kristbaum/como-backend
toolforge jobs run --wait --image tool-como-backend/tool-como-backend:latest --command "migrate" migratejob
toolforge webservice buildservice stop --mount none
toolforge webservice buildservice start --mount none
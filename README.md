# como-backend

A django based, hopefully oauth compatible app server using the toolforge build process.
Modify .env.sample and save it as .env.

## Concept

The database allows for the creation of games. Games have a unique name, a question, a prompt type and a response type.
These games contain prompts which are displayed to players on their devices.
There are three kinds of promt:

* Text: A simple text prompt, e.g Is
* Text-Text: Two texts, e.g Gardenhouse(Text1) Is this a adequate description of this word? (Game-Question) House in the Garden (Text2)
* Image: e.g Image -> Does this depict a person? (Game-Question)
* Image-Text: Does this Person belong in this Category?
* Image-Image: Do these images show the same animal?

For answering there are these three response types:

* Yes/No/Skip
* Text: A textfield: WIP
* Select One (defined at upload, or a constant set) (Maybe select multiple?!): WIP
* Audio recording: Maybe...

## Create a new game from Wikidata

Using this query: <https://qlever.cs.uni-freiburg.de/wikidata/ThnIWH>

## Big Todos

* Create a routine that delets anonymous accounts after a while without any interactions
* Switch to UUIDs (since copying isn't a concern anymore)
* Use proper DB (not SQLite)

## Development build

```bash
sudo add-apt-repository ppa:cncf-buildpacks/pack-cli
sudo apt-get update
sudo apt-get install pack-cli

pack build --builder tools-harbor.wmcloud.org/toolforge/heroku-builder-classic:22 como_backend

docker run -p 8000:8000 --rm --entrypoint web como_backend
```

## Maintenance

```bash
pip install poetry
export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring
poetry self add poetry-plugin-export
poetry update
poetry export -f requirements.txt --output requirements.txt
# Create secret key for django!
```

## Toolforge Deployment commands

```bash
ssh login.toolforge.org
become como-backend
toolforge build start https://gitlab.wikimedia.org/kristbaum/como-backend
toolforge build show
toolforge build logs
toolforge webservice buildservice start --mount none
toolforge envvars create SOCIAL_AUTH_WIKIMEDIA_KEY
toolforge envvars create SOCIAL_AUTH_WIKIMEDIA_SECRET
toolforge envvars create SECRET_KEY
toolforge envvars create DEBUG
toolforge envvars create BASE_URL
toolforge envvars create FRONTEND_URL
toolforge envvars create DATABASE_NAME
toolforge envvars create ADMIN_LIST
toolforge envvars create MATRIX_HOMESERVER_URL
toolforge envvars create MATRIX_ACCESS_TOKEN
toolforge envvars create MATRIX_ADMIN_ROOM
toolforge envvars create MATRIX_COMMON_ROOM


toolforge jobs run --wait --image tool-como-backend/tool-como-backend:latest --command "migrate" migratejob
And then:
python3 como_backend/manage.py create_socialapp
```

Navigate to <https://como-backend.toolforge.org/>

## Toolforge Update Commands

```bash
ssh login.toolforge.org
become como-backend
toolforge build start https://gitlab.wikimedia.org/kristbaum/como-backend
toolforge webservice buildservice start --mount none
toolforge jobs run --wait --image tool-como-backend/tool-como-backend:latest --command "migrate" migratejob
```

or run

```bash
cat update_toolforge.sh | ssh login.toolforge.org
```

## Tests

```bash
python3 como_backend/manage.py test como_backend
```

Add these to docs:
suggestion1
image1_filename_url

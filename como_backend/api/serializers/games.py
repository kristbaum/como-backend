from api.models.games import Game
from api.models.actions import ActionRequestsWithPlaceholders

from rest_framework import serializers


class GameSerializer(serializers.ModelSerializer):
    action_requests_with_placeholders = serializers.SerializerMethodField()
    suggested_csv_headers = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()

    class Meta:
        model = Game
        fields = [
            "id",
            "name",
            "slug",
            "short_description",
            "long_description",
            "language",
            "is_listed",
            "is_active",
            "question_with_jokers",
            "action_requests_with_placeholders",
            "user",
            "confidence_type",
            "created_at",
            "updated_at",
            "prompt_type",
            "prompt_labels_list",
            "response_type",
            "action_type",
            "suggested_csv_headers",
        ]
        read_only_fields = [
            "id",
            "slug",
            "created_at",
            "updated_at",
            "action_requests_with_placeholders",
            "suggested_csv_headers",
            "user",
            "confidence_type",
            "prompt_type",
            "response_type",
            "action_type",
        ]

    def get_action_requests_with_placeholders(self, obj):
        action_requests = ActionRequestsWithPlaceholders.objects.filter(game=obj)

        # Formatting each ActionRequestsWithPlaceholders into a single string
        return [
            f"{ar.request_method}: {ar.request_url}\n{ar.request_body}"
            for ar in action_requests
        ]

    def get_suggested_csv_headers(self, obj):
        return str(obj.get_suggested_csv_headers())

    def get_user(self, obj):
        return obj.user.username

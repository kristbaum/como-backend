from rest_framework import serializers
from api.models.prompts import (
    IPrompt,
    ITPrompt,
    TTPrompt,
    TTTPrompt,
    TTTTPrompt,
)
from api.models.responses import ResponseOption
import urllib.parse


class ResponseOptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResponseOption
        fields = ["text", "link", "sc_id"]


class BasePromptSerializer(serializers.ModelSerializer):
    """Base serializer for all prompt types that includes common logic"""

    response_options = ResponseOptionSerializer(many=True, read_only=True)
    image1_filename = serializers.SerializerMethodField()

    def get_image1_filename(self, obj):
        if obj.image1_filename_url:
            url_parts = obj.image1_filename_url.split("/")
            filename = urllib.parse.unquote(url_parts[-1])
            return filename
        return None


class IPromptSerializer(BasePromptSerializer):
    class Meta:
        model = IPrompt
        fields = [
            "id",
            "question",
            "image1",
            "image1_filename_url",
            "image1_filename",
            "response_options",
        ]


class ITPromptSerializer(BasePromptSerializer):
    class Meta:
        model = ITPrompt
        fields = [
            "id",
            "question",
            "image1",
            "image1_filename_url",
            "image1_filename",
            "text1",
            "link1",
            "response_options",
        ]


class TTPromptSerializer(BasePromptSerializer):
    class Meta:
        model = TTPrompt
        fields = [
            "id",
            "question",
            "text1",
            "link1",
            "text2",
            "link2",
            "response_options",
        ]


class TTTPromptSerializer(BasePromptSerializer):
    class Meta:
        model = TTTPrompt
        fields = [
            "id",
            "question",
            "text1",
            "link1",
            "text2",
            "link2",
            "text3",
            "link3",
            "response_options",
        ]


class TTTTPromptSerializer(BasePromptSerializer):
    class Meta:
        model = TTTTPrompt
        fields = [
            "id",
            "question",
            "text1",
            "link1",
            "text2",
            "link2",
            "text3",
            "link3",
            "text4",
            "link4",
            "response_options",
        ]

import logging
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from api.models.games import Game
from api.models.responses import ComoResponse
from api.permissions import CustomGamePermissions
from api.serializers.games import GameSerializer

from api.single_views.games.creation_logic import GameCreationLogic
from api.single_views.download.download_logic import ComoDownloadLogic
from api.single_views.stat_logic import StatLogic
from rest_framework.decorators import action
from django.db.models import Count
from rest_framework.response import Response
from rest_framework import status


from api.utils import send_matrix_msg
from core.settings import MATRIX_ADMIN_ROOM


class GameViewSet(viewsets.ModelViewSet):
    queryset = Game.objects.filter(is_listed=True)
    serializer_class = GameSerializer
    permission_classes = [
        CustomGamePermissions,
    ]
    lookup_field = "slug"

    def create(self, request, *args, **kwargs):
        """
        Overridden to use the logic from GameCreationLogic.
        """
        creation_logic = GameCreationLogic()
        return creation_logic.post(request, *args, **kwargs)

    @action(detail=False, methods=["get"], url_path="list")
    def list_games(self, request):
        """
        Custom action to list games based on query parameters.
        """
        mode = request.query_params.get("mode", "new")  # Default to 'new'
        include_inactive = (
            request.query_params.get("include_inactive", "false") == "true"
        )

        games = Game.objects.all()

        # Filtering active/inactive games
        if not include_inactive:
            games = games.filter(is_active=True)

        # Filter based on mode
        if mode == "popular":
            games = games.annotate(num_prompts=Count("comoprompt")).order_by(
                "-num_prompts"
            )
        elif mode == "new":
            games = games.order_by("-created_at")
        elif mode == "recent":
            if request.user.is_authenticated:
                recent_responses = ComoResponse.objects.filter(
                    user=request.user
                ).order_by("-created_at")
                game_ids = recent_responses.values_list("prompt__game", flat=True)
                games = games.filter(id__in=game_ids).distinct()
                games = list(games)[::-1]  # Reverse order for recent games
            else:
                games = (
                    Game.objects.none()
                )  # Return empty queryset for unauthenticated users

        # Limit results to 50
        games = games[:50]

        data = GameSerializer(games, many=True).data
        return Response(data=data, status=status.HTTP_200_OK)

    @action(detail=True, methods=["get"])
    def download(self, request, slug=None):
        """
        Custom action to download game data as a CSV file.
        """
        logging.info(f"Starting CSV download for game: {slug}")
        game = get_object_or_404(Game, slug=slug)
        return ComoDownloadLogic().download(game)

    def perform_update(self, serializer):
        # Fetch existing data before updating
        existing_game = Game.objects.get(pk=serializer.instance.pk)

        # Perform the update
        serializer.save()

        # Prepare the message
        game = serializer.instance
        changes = []
        for field in serializer.fields:
            if serializer.fields[field].read_only:
                continue  # Skip read-only fields
            old_value = getattr(existing_game, field)
            new_value = getattr(game, field)
            if old_value != new_value:
                if field == "is_active":
                    action = "activated" if new_value else "deactivated"
                    changes.append(f"🎮 Game '{game.name}' has been {action}.")
                else:
                    changes.append(
                        f"'{field}' changed from '{old_value}' to '{new_value}'."
                    )

        # Join all changes into a single message if there are any
        if changes:
            message = "📝 Update to game '{}':\n{}".format(
                game.name, "\n".join(changes)
            )
            # Send the message to the Matrix chatroom
            send_matrix_msg(MATRIX_ADMIN_ROOM, message)


class StatView(StatLogic):
    pass

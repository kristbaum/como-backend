import logging
import re
import threading
from django.shortcuts import get_object_or_404
from django.http import Http404
import requests

from api.models.prompts import IPrompt, ITPrompt, TTPrompt, TTTPrompt, TTTTPrompt
from core.settings import DEBUG_MODE, MATRIX_ACCESS_TOKEN, MATRIX_HOMESERVER_URL

# def get_access_token(user: User) -> Optional[str]:
#     try:
#         token = SocialToken.objects.get(
#             account__user=user, account__provider="mediawiki"
#         )
#         return token.token
#     except SocialToken.DoesNotExist:
#         return None


def replace_variables(template: str, data: dict) -> str:
    for var in re.findall(r"%([^%]+)%", template):
        if var in data:
            escaped_value = data[var].replace('"', '\\"')
            template = template.replace(f"%{var}%", escaped_value)
    return template


def get_prompt_or_404(prompt_id, game):
    prompt_map = {
        "I": IPrompt,
        "IT": ITPrompt,
        "TT": TTPrompt,
        "TTT": TTTPrompt,
        "TTTT": TTTTPrompt,
    }
    prompt_model = prompt_map.get(game.prompt_type)
    if prompt_model:
        return get_object_or_404(prompt_model, pk=prompt_id)
    logging.error("Prompt type not found")
    raise Http404


def send_matrix_msg(room_id: str, message: str):
    def send_msg():
        """
        Posts a message to a Matrix room.

        :param room_id: The ID of the Matrix room (e.g., '!abcdef:matrix.org')
        :param message: The message to post in the room.
        :return: The response from the Matrix API or an error message.
        """

        if not MATRIX_ACCESS_TOKEN or not MATRIX_HOMESERVER_URL:
            logging.error("Access token is missing")
            return
        if room_id is None or room_id == "!roomid:matrix.org":
            logging.error("Room ID is missing")
            return

        url = f"{MATRIX_HOMESERVER_URL}/_matrix/client/r0/rooms/{room_id}/send/m.room.message"

        headers = {
            "Authorization": f"Bearer {MATRIX_ACCESS_TOKEN}",
            "Content-Type": "application/json",
        }

        data = {
            "msgtype": "m.text",
            "body": ("⚠️ *Test Mode* (ignore): " if DEBUG_MODE else "") + message,
        }

        response = requests.post(url, headers=headers, json=data)

        if response.status_code == 200:
            logging.info("Message posted successfully")
        else:
            logging.info(
                f"Failed to post message: {response.status_code} - {response.text}"
            )

    threading.Thread(target=send_msg).start()

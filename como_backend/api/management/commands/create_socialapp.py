from django.core.management.base import BaseCommand
from allauth.socialaccount.models import SocialApp
from django.conf import settings
from django.contrib.sites.models import Site


class Command(BaseCommand):
    help = (
        "Creates or updates the SocialApp for the mediawiki provider based on settings"
    )

    def handle(self, *args, **kwargs):
        provider_name = "mediawiki"

        # Fetch the provider configuration from settings
        provider_settings = settings.SOCIALACCOUNT_PROVIDERS.get(provider_name, {}).get(
            "APP", {}
        )

        if not provider_settings:
            self.stdout.write(
                self.style.ERROR(f"No settings found for provider: {provider_name}")
            )
            return

        client_id = provider_settings.get("client_id")
        secret = provider_settings.get("secret")

        if not client_id or not secret:
            self.stdout.write(
                self.style.ERROR(
                    f"Missing client_id or secret for provider: {provider_name}"
                )
            )
            return

        # Ensure the Site exists using BASE_URL or FRONTEND_URL
        site, _ = Site.objects.get_or_create(
            domain=settings.BASE_URL, defaults={"name": "Base Site"}
        )

        # Check if the SocialApp already exists
        social_app, created = SocialApp.objects.get_or_create(
            provider=provider_name,
            defaults={
                "name": f"{provider_name.capitalize()} OAuth",
                "client_id": client_id,
                "secret": secret,
            },
        )

        if not created:
            # Update client_id and secret if the app already exists
            social_app.client_id = client_id
            social_app.secret = secret
            social_app.save()

        # Ensure the app is associated with the site
        social_app.sites.add(site)

        self.stdout.write(
            self.style.SUCCESS(
                f"SocialApp for provider '{provider_name}' has been created or updated."
            )
        )

import logging
from rest_framework.permissions import SAFE_METHODS, BasePermission


class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class IsWikimediaUser(BasePermission):
    """
    Allows access only to users who have connected a Wikimedia OAuth account.
    """

    def has_permission(self, request, view):
        # Check if the user has a Wikimedia OAuth account
        return request.user.is_authenticated and request.user.is_wikimedia_user


class IsOwnerOrReadOnly(BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        return obj.user == request.user


class CustomGamePermissions(BasePermission):
    """
    Custom permissions class to handle game object permissions.
    """

    def has_object_permission(self, request, view, obj):
        # Allow delete only to admins and the owner of the game
        if view.action == "destroy":
            return request.user.is_staff or request.user == obj.user

        # Check if the user is trying to update the `is_active` field
        if "is_active" in request.data:
            return request.user.is_staff  # Only allow admins to update `is_active`

        # Check for other field updates
        if set(request.data.keys()) - {"is_active"}:
            return (
                request.user == obj.user
            )  # Only allow the game's user to update other fields

        return True  # Allow read-only access for everyone else

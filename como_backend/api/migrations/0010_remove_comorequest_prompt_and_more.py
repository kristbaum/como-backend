# Generated by Django 5.0.6 on 2024-06-02 01:17

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0009_alter_game_action_type_alter_game_evidence_level_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comorequest',
            name='prompt',
        ),
        migrations.RemoveField(
            model_name='game',
            name='number_of_requests_pro_prompt',
        ),
        migrations.AddField(
            model_name='comoprompt',
            name='custom_id',
            field=models.CharField(max_length=500, null=True),
        ),
        migrations.CreateModel(
            name='ActionRequest',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('request_method', models.CharField(choices=[('PUT', 'PUT'), ('PATCH', 'PATCH'), ('POST', 'POST')], max_length=5)),
                ('request_url', models.URLField()),
                ('request_body', models.CharField(max_length=1000)),
                ('prompt', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.comoprompt')),
            ],
        ),
        migrations.CreateModel(
            name='ActionRequestsWithPlaceholders',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('request_method', models.CharField(choices=[('PUT', 'PUT'), ('PATCH', 'PATCH'), ('POST', 'POST')], max_length=5)),
                ('request_url', models.URLField()),
                ('request_body_with_jokers', models.CharField(max_length=1000)),
                ('game', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.game')),
            ],
        ),
        migrations.DeleteModel(
            name='ActionRequestsWithJokers',
        ),
        migrations.DeleteModel(
            name='ComoRequest',
        ),
    ]

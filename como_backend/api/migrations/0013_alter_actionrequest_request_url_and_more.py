# Generated by Django 5.0.6 on 2024-06-03 22:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_alter_game_action_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actionrequest',
            name='request_url',
            field=models.URLField(max_length=1000),
        ),
        migrations.AlterField(
            model_name='actionrequestswithplaceholders',
            name='request_url',
            field=models.URLField(max_length=1000),
        ),
        migrations.AlterField(
            model_name='itprompt',
            name='image1',
            field=models.URLField(max_length=1000),
        ),
        migrations.AlterField(
            model_name='itprompt',
            name='image1_filename_url',
            field=models.URLField(max_length=1000),
        ),
        migrations.AlterField(
            model_name='itprompt',
            name='link1',
            field=models.URLField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='ttprompt',
            name='link1',
            field=models.URLField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='ttprompt',
            name='link2',
            field=models.URLField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='tttprompt',
            name='link1',
            field=models.URLField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='tttprompt',
            name='link2',
            field=models.URLField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='tttprompt',
            name='link3',
            field=models.URLField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='ttttprompt',
            name='link1',
            field=models.URLField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='ttttprompt',
            name='link2',
            field=models.URLField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='ttttprompt',
            name='link3',
            field=models.URLField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='ttttprompt',
            name='link4',
            field=models.URLField(max_length=1000, null=True),
        ),
    ]

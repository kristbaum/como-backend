# Generated by Django 5.0.6 on 2024-06-09 00:15

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0015_iprompt_responseoption'),
    ]

    operations = [
        migrations.AlterField(
            model_name='responseoption',
            name='prompt',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='response_option', to='api.comoprompt'),
        ),
    ]

from django.contrib import admin
from api.models.games import Game
from api.models.prompts import ComoPrompt
from api.models import ComoUser

# Register your models here.
registered_models = [ComoUser, Game, ComoPrompt]
admin.site.register(registered_models)

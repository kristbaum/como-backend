from unittest.mock import patch
from django.test import TestCase
from rest_framework import status
from api.models.games import Game
from api.single_views.games.creation_check import (
    check_csv_completeness,
    check_json_completness,
)
from api.models.users import ComoUser


class CheckJsonCompletenessTests(TestCase):
    def setUp(self):
        self.user = ComoUser.objects.create(
            username="testuser", email="test@example.com"
        )

        self.game = Game.objects.create(
            name="Chess",
            slug="chess",
            short_description="Test",
            long_description="Test Game",
            language="en",
            is_listed=True,
            is_active=False,
            question_with_jokers="Who won?",
            confidence_type="S",
            prompt_type="TT",
            response_type="SC",
            user=self.user,  # Assign the created user here
        )

    def test_missing_required_fields(self):
        json_data = {
            "name": "Chess",
            "prompt_type": "TT",
            "response_type": "SC",
            # Missing other required fields
        }
        response = check_json_completness(json_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("Missing required JSON fields", response.data)

    def test_complete_data(self):
        json_data = {
            "name": "Chess",
            "prompt_type": "TT",
            "response_type": "SC",
            "short_description": "Short",
            "long_description": "Long",
            "action_type": "none",
            "question": "What's next?",
            "content": "data",
            "is_listed": True,
            "confidence_type": "S",
        }
        response = check_json_completness(json_data)
        self.assertIsNone(response)  # None means no errors

    def test_action_type_wa_without_requests(self):
        json_data = {
            "name": "Chess",
            "prompt_type": "TT",
            "response_type": "SC",
            "short_description": "Short",
            "long_description": "Long",
            "action_type": "WA",
            "question": "What's next?",
            "content": "data",
            "is_listed": True,
            "confidence_type": "S",
        }
        response = check_json_completness(json_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("no action_requests included", response.data)


class CheckCsvCompletenessTests(TestCase):
    def setUp(self):
        self.user = ComoUser.objects.create(
            username="testuser", email="test@example.com"
        )
        self.game = Game.objects.create(
            name="Chess",
            slug="chess",
            short_description="Test",
            long_description="Test Game",
            language="en",
            is_listed=True,
            is_active=False,
            question_with_jokers="Who won?",
            confidence_type="S",
            prompt_type="TT",
            response_type="SC",
            user=self.user,
        )

    @patch("csv.DictReader")
    def test_csv_missing_required_fields(self, mock_csv_reader):
        mock_csv_reader.return_value.fieldnames = ["text1"]
        response = check_csv_completeness(self.game, mock_csv_reader)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("Missing required field(s)", response.data["error"])

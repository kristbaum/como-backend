from django.test import TestCase
from django.contrib.auth import get_user_model
from api.models import ComoUser
from api.models.games import Game

User = get_user_model()


class GameModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create a test user
        cls.user = ComoUser.objects.create(
            username="testuser", email="test@example.com", is_wikimedia_user=True
        )

        # Create a game instance
        cls.game = Game.objects.create(
            name="Test Game",
            slug="test-game",
            short_description="Test short description",
            long_description="Test long description",
            language="en",
            is_listed=True,
            is_active=False,
            question_with_jokers="What's the main character's motive?",
            user=cls.user,
            confidence_type="S",
            prompt_type="I",
            response_type="SC",
            action_type="QS",
        )

    def test_game_creation(self):
        self.assertEqual(self.game.name, "Test Game")
        self.assertTrue(self.game.is_listed)
        self.assertFalse(self.game.is_active)
        self.assertEqual(self.game.language, "en")
        self.assertEqual(self.game.user, self.user)
        self.assertEqual(self.game.confidence_type, "S")
        self.assertEqual(self.game.prompt_type, "I")
        self.assertEqual(self.game.response_type, "SC")
        self.assertEqual(self.game.action_type, "QS")

    def test_game_str(self):
        self.assertEqual(str(self.game), "Test Game")

    def test_field_defaults(self):
        # Test default values are set correctly
        game = Game.objects.create(
            name="Default Game", slug="default-game", user=self.user
        )
        self.assertTrue(game.is_listed)  # Default is True
        self.assertFalse(game.is_active)  # Default is False
        self.assertEqual(game.language, "en")  # Default language

    def test_unique_slug(self):
        # Testing that slug field is indeed unique
        Game.objects.create(name="Unique Game", slug="unique-game", user=self.user)
        with self.assertRaises(Exception):
            Game.objects.create(name="Another Game", slug="unique-game", user=self.user)

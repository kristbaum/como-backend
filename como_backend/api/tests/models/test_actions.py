from django.test import TestCase
from api.models.games import Game
from api.models.prompts import ComoPrompt
from api.models.actions import ActionRequest, ActionRequestsWithPlaceholders
from api.models.users import ComoUser


class ActionRequestTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create instances of Game and ComoPrompt for ForeignKey relationships
        cls.user = ComoUser.objects.create_user(
            username="testuser", email="test@example.com", password="testpass123"
        )

        # Ensure the user is created and then create a game instance linked to this user
        cls.game = Game.objects.create(
            name="Chess", slug="chess", user=cls.user  # Link the created user here
        )
        cls.prompt = ComoPrompt.objects.create(
            game=cls.game, question="What is the best move?"
        )

    def test_action_requests_with_placeholders_creation(self):
        action = ActionRequestsWithPlaceholders.objects.create(
            game=self.game,
            request_method="POST",
            request_url="http://example.com/api/action",
            request_body='{"key": "value"}',
        )
        self.assertEqual(action.game, self.game)
        self.assertEqual(action.request_method, "POST")
        self.assertEqual(action.request_url, "http://example.com/api/action")
        self.assertEqual(action.request_body, '{"key": "value"}')

    def test_action_request_creation(self):
        action = ActionRequest.objects.create(
            prompt=self.prompt,
            request_method="PUT",
            request_url="http://example.com/api/update",
            request_body='{"update": "data"}',
        )
        self.assertEqual(action.prompt, self.prompt)
        self.assertEqual(action.request_method, "PUT")
        self.assertEqual(action.request_url, "http://example.com/api/update")
        self.assertEqual(action.request_body, '{"update": "data"}')

from django.test import TestCase
from django.utils import timezone
from api.models.games import Game
from api.models.prompts import (
    ComoPrompt,
    ITPrompt,
    IPrompt,
    TTPrompt,
    TTTPrompt,
    TTTTPrompt,
)
from api.models.users import ComoUser


class ComoPromptModelTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create a user instance for associating with a game
        cls.user = ComoUser.objects.create_user(
            username="testuser", email="test@example.com", password="testpass123"
        )

        # Ensure the user is created and then create a game instance linked to this user
        cls.game = Game.objects.create(
            name="Chess", slug="chess", user=cls.user  # Link the created user here
        )

    def create_como_prompt(self, done=False):
        return ComoPrompt.objects.create(
            game=self.game,
            question="What is the best opening?",
            done=done,
            skip_count=0,
        )

    def test_como_prompt_creation(self):
        prompt = self.create_como_prompt()
        self.assertFalse(prompt.done)
        self.assertEqual(prompt.skip_count, 0)
        self.assertEqual(prompt.game, self.game)

    def test_como_prompt_done_at(self):
        prompt = self.create_como_prompt(done=True)
        prompt.done_at = timezone.now()
        prompt.save()
        self.assertIsNotNone(prompt.done_at)

    def test_it_prompt_creation(self):
        prompt = ITPrompt.objects.create(
            game=self.game,
            question="Identify the structure",
            image1="http://example.com/image1.jpg",
            image1_filename_url="http://example.com/fileimage1.jpg",
            text1="Describe this structure",
            link1="http://example.com/link1",
        )
        self.assertEqual(prompt.text1, "Describe this structure")
        self.assertIsNotNone(prompt.link1)

    def test_i_prompt_creation(self):
        prompt = IPrompt.objects.create(
            game=self.game,
            question="Identify the monument",
            image1="http://example.com/monument.jpg",
            image1_filename_url="http://example.com/filemonument.jpg",
        )
        self.assertIn("monument.jpg", prompt.image1)

    def test_tt_prompt_creation(self):
        prompt = TTPrompt.objects.create(
            game=self.game,
            question="Compare two theories",
            text1="Theory one description",
            link1="http://example.com/theory1",
            text2="Theory two description",
            link2="http://example.com/theory2",
        )
        self.assertEqual(prompt.text2, "Theory two description")

    def test_ttt_prompt_creation(self):
        prompt = TTTPrompt.objects.create(
            game=self.game,
            question="Explain three concepts",
            text1="Concept 1",
            text2="Concept 2",
            text3="Concept 3",
        )
        self.assertEqual(prompt.text3, "Concept 3")

    def test_tttt_prompt_creation(self):
        prompt = TTTTPrompt.objects.create(
            game=self.game,
            question="Detail four points",
            text1="Point 1",
            text2="Point 2",
            text3="Point 3",
            text4="Point 4",
        )
        self.assertEqual(prompt.text4, "Point 4")

from django.test import TestCase
from api.models import ComoUser
from api.models.games import Game


class GameModelTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create a user instance for associating with a game
        cls.user = ComoUser.objects.create_user(
            username="testuser", email="test@example.com", password="testpass123"
        )

        cls.game = Game.objects.create(
            name="Chess",
            slug="chess",
            short_description="A classic strategy game",
            long_description="Detailed description of the game rules and history",
            language="en",
            is_listed=True,
            is_active=False,
            question_with_jokers="Who is the world champion?",
            user=cls.user,
            confidence_type="S",
            prompt_type="TT",
            prompt_labels_list="first move, best move",
            response_type="SC",
            action_type="QS",
        )

    def test_game_creation(self):
        # Test all fields are correctly created
        self.assertEqual(self.game.name, "Chess")
        self.assertEqual(self.game.slug, "chess")
        self.assertEqual(self.game.short_description, "A classic strategy game")
        self.assertEqual(
            self.game.long_description,
            "Detailed description of the game rules and history",
        )
        self.assertEqual(self.game.language, "en")
        self.assertTrue(self.game.is_listed)
        self.assertFalse(self.game.is_active)
        self.assertEqual(self.game.question_with_jokers, "Who is the world champion?")
        self.assertEqual(self.game.user, self.user)
        self.assertEqual(self.game.confidence_type, "S")
        self.assertEqual(self.game.prompt_type, "TT")
        self.assertEqual(self.game.prompt_labels_list, "first move, best move")
        self.assertEqual(self.game.response_type, "SC")
        self.assertEqual(self.game.action_type, "QS")

    def test_str_method(self):
        # Test the custom string method
        self.assertEqual(str(self.game), "Chess")

    def test_game_defaults(self):
        # Test default values
        new_game = Game.objects.create(
            name="New Game",
            slug="new-game",
            short_description="A new puzzle game",
            long_description="Some long description",
            user=self.user,
        )
        self.assertTrue(new_game.is_listed)
        self.assertFalse(new_game.is_active)
        self.assertEqual(new_game.language, "en")
        self.assertEqual(new_game.confidence_type, "S")

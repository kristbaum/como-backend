from django.test import TestCase
from django.contrib.auth import get_user_model

ComoUser = get_user_model()


class ComoUserModelTests(TestCase):
    def test_default_language(self):
        """Test that the default language is set correctly for new users."""
        user = ComoUser.objects.create_user(
            username="testuser", email="test@example.com"
        )
        self.assertEqual(user.language, "en")

    def test_language_field(self):
        """Test that the language field can be set and retrieved correctly."""
        user = ComoUser.objects.create_user(
            username="testuser2", email="test2@example.com", language="es"
        )
        self.assertEqual(user.language, "es")

    def test_default_is_wikimedia_user(self):
        """Test that the is_wikimedia_user field defaults to False."""
        user = ComoUser.objects.create_user(
            username="testuser3", email="test3@example.com"
        )
        self.assertFalse(user.is_wikimedia_user)

    def test_is_wikimedia_user_field(self):
        """Test that the is_wikimedia_user field can be set and retrieved correctly."""
        user = ComoUser.objects.create_user(
            username="testuser4", email="test4@example.com", is_wikimedia_user=True
        )
        self.assertTrue(user.is_wikimedia_user)

    def test_str_method(self):
        """Test the custom string method if implemented."""
        user = ComoUser(username="testuser5", email="test5@example.com")
        self.assertEqual(str(user), "testuser5")

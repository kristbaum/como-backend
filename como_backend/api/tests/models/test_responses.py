from django.test import TestCase
from django.contrib.auth import get_user_model
from api.models.prompts import ComoPrompt
from api.models import (
    ComoUser,
)
from api.models.responses import (
    ComoResponse,
    ResponseOption,
    SingleChoiceResponse,
    TextResponse,
    YesNoResponse,
)
from api.models.games import Game

ComoUser = get_user_model()


class ResponseModelTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create a user and prompt instance to use for responses
        cls.user = ComoUser.objects.create_user(
            username="testuser", email="test@example.com", password="testpass123"
        )

        # Create a game instance linked to the user
        cls.game = Game.objects.create(name="Chess", slug="chess", user=cls.user)

        # Create a prompt linked to the game
        cls.prompt = ComoPrompt.objects.create(
            game=cls.game, question="What is the capital of France?"
        )

    def test_como_response_creation(self):
        response = ComoResponse.objects.create(
            prompt=self.prompt, user=self.user, skipped=False
        )
        self.assertFalse(response.skipped)
        self.assertEqual(response.prompt, self.prompt)
        self.assertEqual(response.user, self.user)

    def test_yes_no_response(self):
        yes_no_response = YesNoResponse.objects.create(
            prompt=self.prompt, user=self.user, answer_is_yes=True, skipped=False
        )
        self.assertTrue(yes_no_response.answer_is_yes)

    def test_text_response(self):
        text_response = TextResponse.objects.create(
            prompt=self.prompt,
            user=self.user,
            text="Paris",
            same_as_prompt=False,
            skipped=False,
        )
        self.assertEqual(text_response.text, "Paris")
        self.assertFalse(text_response.same_as_prompt)

    def test_single_choice_response(self):
        option = ResponseOption.objects.create(
            prompt=self.prompt, text="Paris", link="http://example.com"
        )
        single_choice_response = SingleChoiceResponse.objects.create(
            prompt=self.prompt, user=self.user, selected=option, skipped=False
        )
        self.assertEqual(single_choice_response.selected, option)

    def test_response_option(self):
        option = ResponseOption.objects.create(
            prompt=self.prompt, text="Paris", link="http://example.com"
        )
        self.assertEqual(option.text, "Paris")
        self.assertEqual(option.link, "http://example.com")
        self.assertEqual(option.prompt, self.prompt)

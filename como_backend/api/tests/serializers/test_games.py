from django.test import TestCase
from django.contrib.auth import get_user_model
from api.models.games import Game
from api.models.actions import ActionRequestsWithPlaceholders
from api.serializers.games import GameSerializer

User = get_user_model()


class GameSerializerTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create a user for associating with a game
        cls.user = User.objects.create_user(username="testuser", password="testpass")

        # Create a game instance
        cls.game = Game.objects.create(
            name="Chess",
            slug="chess",
            user=cls.user,
            short_description="A classic strategy game",
            long_description="Detailed description of the game rules and history",
            language="en",
            is_listed=True,
            is_active=True,
            question_with_jokers="Who is the current world champion?",
        )

        # Create some action requests linked to the game
        ActionRequestsWithPlaceholders.objects.create(
            game=cls.game,
            request_method="POST",
            request_url="http://example.com/api/action",
            request_body='{"action": "create"}',
        )

    def test_serialize_game(self):
        # Serialize the game instance
        serializer = GameSerializer(instance=self.game)

        # Check that all fields are correctly serialized
        expected_fields = {
            "id",
            "name",
            "slug",
            "short_description",
            "long_description",
            "language",
            "is_listed",
            "is_active",
            "question_with_jokers",
            "action_requests_with_placeholders",
            "user",
            "confidence_type",
            "created_at",
            "updated_at",
            "prompt_type",
            "prompt_labels_list",
            "response_type",
            "action_type",
            "suggested_csv_headers",
        }
        self.assertEqual(set(serializer.data.keys()), expected_fields)

    def test_action_requests_with_placeholders_field(self):
        serializer = GameSerializer(instance=self.game)
        action_requests_data = serializer.data["action_requests_with_placeholders"]

        # Verify that the action_requests_with_placeholders field contains correct data
        expected_data = ['POST: http://example.com/api/action\n{"action": "create"}']
        self.assertEqual(action_requests_data, expected_data)

    def test_user_field(self):
        serializer = GameSerializer(instance=self.game)
        user_data = serializer.data["user"]

        # Verify that the user field formats the URL correctly
        expected_url = f"{self.game.user.username}"
        self.assertEqual(user_data, expected_url)

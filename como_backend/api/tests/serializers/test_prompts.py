from django.test import TestCase
from api.models.prompts import (
    ComoPrompt,
    IPrompt,
    ITPrompt,
    TTPrompt,
    TTTPrompt,
    TTTTPrompt,
)
from api.models.responses import ResponseOption
from api.serializers.prompts import (
    IPromptSerializer,
    ITPromptSerializer,
    TTPromptSerializer,
    TTTPromptSerializer,
    TTTTPromptSerializer,
)
from api.models.games import Game
from api.models.users import ComoUser


class PromptSerializerTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # This method sets up data for all tests in this class
        cls.user = ComoUser.objects.create_user(
            username="testuser", email="test@example.com", password="testpass123"
        )
        cls.game = Game.objects.create(
            name="Chess", slug="chess", user=cls.user  # Link the created user here
        )
        cls.prompt = ComoPrompt.objects.create(
            game=cls.game, question="What is the capital of France?"
        )
        cls.response_option = ResponseOption.objects.create(
            text="Option 1", link="http://example.com", sc_id=1, prompt=cls.prompt
        )

        # Create instances for each prompt type
        cls.iprompt = IPrompt.objects.create(
            game=cls.game,
            question="Identify this object",
            image1_filename_url="http://example.com/image1.jpg",
        )
        cls.itprompt = ITPrompt.objects.create(
            game=cls.game,
            question="Identify and describe this object",
            image1_filename_url="http://example.com/image1.jpg",
            text1="Description",
        )
        cls.ttprompt = TTPrompt.objects.create(
            game=cls.game,
            question="Compare these two concepts",
            text1="Concept 1",
            link1="http://example.com/link1",
            text2="Concept 2",
            link2="http://example.com/link2",
        )
        cls.tttprompt = TTTPrompt.objects.create(
            game=cls.game,
            question="Discuss these three items",
            text1="Item 1",
            link1="http://example.com/link1",
            text2="Item 2",
            link2="http://example.com/link2",
            text3="Item 3",
            link3="http://example.com/link3",
        )
        cls.ttttprompt = TTTTPrompt.objects.create(
            game=cls.game,
            question="Analyze these four elements",
            text1="Element 1",
            link1="http://example.com/link1",
            text2="Element 2",
            link2="http://example.com/link2",
            text3="Element 3",
            link3="http://example.com/link3",
            text4="Element 4",
            link4="http://example.com/link4",
        )

    def test_iprompt_serializer(self):
        serializer = IPromptSerializer(instance=self.iprompt)
        self.assertEqual(serializer.data["question"], self.iprompt.question)
        self.assertIn("image1_filename", serializer.data)

    def test_itprompt_serializer(self):
        serializer = ITPromptSerializer(instance=self.itprompt)
        self.assertEqual(serializer.data["text1"], "Description")
        self.assertIn("image1_filename", serializer.data)

    def test_ttprompt_serializer(self):
        serializer = TTPromptSerializer(instance=self.ttprompt)
        self.assertEqual(serializer.data["text2"], "Concept 2")
        self.assertIn("link2", serializer.data)

    def test_tttprompt_serializer(self):
        serializer = TTTPromptSerializer(instance=self.tttprompt)
        self.assertEqual(serializer.data["text3"], "Item 3")
        self.assertIn("link3", serializer.data)

    def test_ttttprompt_serializer(self):
        serializer = TTTTPromptSerializer(instance=self.ttttprompt)
        self.assertEqual(serializer.data["text4"], "Element 4")
        self.assertIn("link4", serializer.data)

    def test_get_image1_filename(self):
        serializer = IPromptSerializer(instance=self.iprompt)
        expected_filename = (
            "image1.jpg"  # Assuming the file name is extracted correctly from the URL
        )
        self.assertEqual(
            serializer.get_image1_filename(self.iprompt), expected_filename
        )

    def test_response_option_serialization(self):
        # Testing nested serialization of response options
        serializer = TTPromptSerializer(instance=self.ttprompt)
        response_option = ResponseOption.objects.create(
            text="Compare option",
            link="http://compare.com",
            sc_id=2,
            prompt=self.ttprompt,
        )
        expected = [
            {
                "text": response_option.text,
                "link": response_option.link,
                "sc_id": response_option.sc_id,
            }
        ]
        self.assertEqual(serializer.data["response_options"], expected)

import logging
from typing import Type
from django.shortcuts import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.exceptions import NotAuthenticated

from api.single_views.como_action_logic import evaluate_action
from api.utils import get_prompt_or_404
from django.db import transaction

from api.models.games import Game
from api.models.prompts import ComoPrompt
from api.models.responses import (
    ComoResponse,
    ResponseOption,
    SingleChoiceResponse,
    TextResponse,
    YesNoResponse,
)
from api.models.users import ComoUser
from core.settings import DRY_RUN

"""Como Response Logic Overview:
Which Type of response? -> Record response -> Are conditions for done met? -> Execute Action"""


class ComoResponseLogic(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, game_slug, prompt_id, format=None):
        json_data = request.data
        game = get_object_or_404(Game, slug=game_slug)
        if not game.is_active:
            return Response("Game is not active", status=status.HTTP_404_NOT_FOUND)

        if game.confidence_type in ["L", "E"] and not request.user.is_wikimedia_user:
            logging.error(f"Unauthorized user tried to respond in game: {game}")
            return Response(
                "User is not authenticated", status=status.HTTP_403_FORBIDDEN
            )

        prompt = get_prompt_or_404(prompt_id, game)
        if prompt.done:
            return Response(
                "The submitted prompt has already been done",
                status=status.HTTP_410_GONE,
            )

        logging.info(f"Processing request for game: {game}")
        # Selects the right response type method
        response_method = getattr(
            self, f"_process_{game.response_type.lower()}_response", None
        )

        if response_method:
            return response_method(request.user, json_data, game, prompt)
        else:
            return Response(
                "Response type not supported", status=status.HTTP_400_BAD_REQUEST
            )

    def _validate_required_fields(self, data: dict, fields: list) -> bool:
        return all(field in data for field in fields)

    @transaction.atomic
    def _process_yn_response(
        self, user: ComoUser, data: dict, game: Game, prompt: Type[ComoPrompt]
    ) -> Response:

        if not self._validate_required_fields(data, ["skipped", "answer_is_yes"]):
            return Response(
                "Missing required JSON fields", status=status.HTTP_400_BAD_REQUEST
            )

        yes_no_response = YesNoResponse.objects.create(
            prompt=prompt,
            user=user,
            skipped=data["skipped"],
            answer_is_yes=data.get("answer_is_yes"),
        )
        return self._finalize_response(game, prompt, yes_no_response)

    @transaction.atomic
    def _process_et_response(
        self, user: ComoUser, data: dict, game: Game, prompt: Type[ComoPrompt]
    ) -> Response:
        if not self._validate_required_fields(data, ["skipped", "text"]):
            return Response(
                "Missing required JSON fields", status=status.HTTP_400_BAD_REQUEST
            )

        # Get the original suggestion (There should only be one)
        responseOption = get_object_or_404(ResponseOption, prompt=prompt)

        text_response = TextResponse.objects.create(
            prompt=prompt,
            user=user,
            skipped=data["skipped"],
            text=data.get("text"),
            same_as_prompt=(responseOption.text == data.get("text")),
        )
        return self._finalize_response(game, prompt, text_response)

    @transaction.atomic
    def _process_sc_response(
        self, user: ComoUser, data: dict, game: Game, prompt: Type[ComoPrompt]
    ) -> Response:
        if not self._validate_required_fields(data, ["skipped", "selected"]):
            return Response(
                "Missing required JSON fields", status=status.HTTP_400_BAD_REQUEST
            )

        # Get the original suggestion
        responseOption = get_object_or_404(
            ResponseOption, prompt=prompt, sc_id=data["selected"]
        )

        response = SingleChoiceResponse.objects.create(
            prompt=prompt, user=user, skipped=data["skipped"], selected=responseOption
        )
        return self._finalize_response(game, prompt, response)

    def _finalize_response(
        self, game: Game, prompt: ComoPrompt, response: Type[ComoResponse]
    ) -> Response:
        if self._evaluate_completion(prompt, game.confidence_type, response):
            try:
                if game.action_type and not DRY_RUN:
                    evaluate_action(game, prompt, response, response.user)
            except NotAuthenticated as e:
                return Response(
                    "Failed to trigger action, probably out of date OAUTH",
                    status=status.HTTP_403_FORBIDDEN,
                )

        return Response("Response created successfully", status=status.HTTP_201_CREATED)

    def _evaluate_completion(
        self, prompt: ComoPrompt, confidence_type: str, response: Type[ComoResponse]
    ) -> bool:
        # If skipped the prompt is not done
        if response.skipped:
            prompt.skip_count += 1
            prompt.save()
            return False
        if confidence_type == "S" or confidence_type == "L":
            return set_done(prompt, response)
        else:
            logging.info("Evaluating double confirmation prompt")
            if type(response) == YesNoResponse:
                if (
                    YesNoResponse.objects.filter(
                        prompt=prompt, skipped=False, answer_is_yes=True
                    ).count()
                    >= 1
                    and response.answer_is_yes
                ):
                    return set_done(prompt, response)
                elif (
                    YesNoResponse.objects.filter(
                        prompt=prompt, skipped=False, answer_is_yes=False
                    ).count()
                    >= 1
                    and not response.answer_is_yes
                ):
                    return set_done(prompt, response)
                else:
                    logging.info("Prompt not done yet")
                    return False
            return set_done(prompt, response)


def set_done(prompt: Type[ComoPrompt], response: Type[ComoResponse]) -> bool:
    logging.info("Setting prompt to done")
    prompt.done = True
    prompt.done_at = response.created_at
    prompt.save()
    return True

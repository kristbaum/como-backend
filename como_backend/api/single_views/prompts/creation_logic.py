import csv
import gzip
import io
import json
import logging
import time
from typing import Any, Dict, List, Optional, Tuple, Type

from django.db import transaction
from django.http import HttpRequest
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models.actions import ActionRequest
from api.models.games import Game
from api.models.prompts import (
    ComoPrompt,
    IPrompt,
    ITPrompt,
    TTPrompt,
    TTTPrompt,
    TTTTPrompt,
)
from api.models.responses import ResponseOption
from api.permissions import IsWikimediaUser
from api.single_views.games.creation_check import check_csv_completeness
from api.utils import replace_variables


class PromptCreationLogic(APIView):
    permission_classes = [IsWikimediaUser]

    def post(self, request, game_slug, format=None):
        game = get_object_or_404(Game, slug=game_slug)
        logging.info(f"Adding prompts to game {game}")

        if not request.user.is_staff or request.user!=game.user:
            return Response(
                "User is not authorized to add prompts to this game",
                status=status.HTTP_403_FORBIDDEN,
            )

        json_data = self._parse_request_data(request)
        if json_data is None:
            return Response(
                "Invalid or corrupted data", status=status.HTTP_400_BAD_REQUEST
            )

        # Retrieve and process the CSV content from JSON data
        file_like_object = io.StringIO(json_data["data"])
        reader = csv.DictReader(file_like_object)

        logging.info("Checking CSV data")
        check_csv_completeness(game, reader)

        # Create prompts and action requests from CSV data
        prompts, action_requests, response_options = self.create_prompts(
            json_data, reader, game
        )

        with transaction.atomic():
            self._save_prompts(prompts, action_requests, response_options)

        return Response(
            status=status.HTTP_201_CREATED, data="Prompts added successfully"
        )

    def _parse_request_data(self, request: HttpRequest) -> Optional[Dict[str, Any]]:
        if request.headers.get("Content-Encoding") == "gzip":
            try:
                return json.loads(gzip.decompress(request.body))
            except gzip.error:
                logging.error("Failed to decompress gzip data")
                return None
        return request.data

    def _save_prompts(
        self,
        prompts: List[Type[ComoPrompt]],
        como_requests: List[ActionRequest],
        response_options: List[ResponseOption],
    ) -> None:
        self._save_objects_and_log_progress(prompts, "prompts")
        self._save_objects_and_log_progress(como_requests, "como_requests")
        self._save_objects_and_log_progress(response_options, "response_options")

    def _save_objects_and_log_progress(
        self, objects: List[Any], description: str
    ) -> None:
        start_time = time.time()
        for i, obj in enumerate(objects):
            obj.save()
            if (i + 1) % 500 == 0:
                elapsed_time = time.time() - start_time
                logging.info(
                    f"Saved {i + 1} {description}. Elapsed time: {elapsed_time} seconds"
                )
        elapsed_time = time.time() - start_time
        logging.info(
            f"Saved {len(objects)} {description}. Total elapsed time: {elapsed_time} seconds"
        )

    def _get_link_or_none(self, row: dict, key: str) -> str:
        return row.get(key) if row.get(key) != "" else None

    def create_prompts(
        self, json_data: dict, reader: csv.DictReader, game: Game
    ) -> Tuple[List[Type[ComoPrompt]], List[ActionRequest], List[ResponseOption]]:
        prompts = []
        como_requests = []
        response_options = []
        for row in reader:
            prompt = None
            if game.prompt_type == "TT":
                prompt = TTPrompt(
                    game=game,
                    custom_id=row.get("custom_id"),
                    question=replace_variables(game.question_with_jokers, row),
                    text1=row["text1"],
                    link1=self._get_link_or_none(row, "link1"),
                    text2=row["text2"],
                    link2=self._get_link_or_none(row, "link2"),
                )

            elif game.prompt_type == "TTT":
                prompt = TTTPrompt(
                    game=game,
                    custom_id=row.get("custom_id"),
                    question=replace_variables(game.question_with_jokers, row),
                    text1=row["text1"],
                    link1=self._get_link_or_none(row, "link1"),
                    text2=row["text2"],
                    link2=self._get_link_or_none(row, "link2"),
                    text3=row["text3"],
                    link3=self._get_link_or_none(row, "link3"),
                )

            elif game.prompt_type == "TTTT":
                prompt = TTTTPrompt(
                    game=game,
                    custom_id=row.get("custom_id"),
                    question=replace_variables(game.question_with_jokers, row),
                    text1=row["text1"],
                    link1=self._get_link_or_none(row, "link1"),
                    text2=row["text2"],
                    link2=self._get_link_or_none(row, "link2"),
                    text3=row["text3"],
                    link3=self._get_link_or_none(row, "link3"),
                    text4=row["text4"],
                    link4=self._get_link_or_none(row, "link4"),
                )

            elif game.prompt_type == "I":
                prompt = IPrompt(
                    game=game,
                    custom_id=row.get("custom_id"),
                    question=replace_variables(game.question_with_jokers, row),
                    image1=row["image1"],
                    image1_filename_url=row["image1_filename_url"],
                )

            elif game.prompt_type == "IT":
                prompt = ITPrompt(
                    game=game,
                    custom_id=row.get("custom_id"),
                    question=replace_variables(game.question_with_jokers, row),
                    text1=row["text1"],
                    link1=self._get_link_or_none(row, "link1"),
                    image1=row["image1"],
                    image1_filename_url=row["image1_filename_url"],
                )

            prompts.append(prompt)

            if game.response_type == "SC":
                # Only 4 options max TODO: Document this
                for i in range(1, 5):
                    try:
                        # ignore empty rows
                        if row[f"option{i}"] != "":
                            response_option = ResponseOption(
                                prompt=prompt,
                                sc_id=i,
                                text=row[f"option{i}"],
                                link=self._get_link_or_none(row, f"option{i}link"),
                            )
                            response_options.append(response_option)
                    except KeyError:
                        logging.info("No more response options after: " + str(i))
                        break
            elif game.response_type == "ET":
                response_option = ResponseOption(prompt=prompt, text=row["suggestion1"])
                response_options.append(response_option)

            if "action_requests" in json_data:
                action_requests = json.loads(json_data["action_requests"])
                for action_request in action_requests:

                    como_request = ActionRequest(
                        prompt=prompt,
                        request_method=action_request["request_http_method"],
                        request_url=replace_variables(
                            action_request["request_url"], row
                        ),
                        request_body=replace_variables(
                            action_request["request_body"], row
                        ),
                    )
                    como_requests.append(como_request)
        return prompts, como_requests, response_options

import logging
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from api.models.prompts import IPrompt, ITPrompt, TTPrompt, TTTPrompt, TTTTPrompt
from api.serializers.prompts import (
    IPromptSerializer,
    ITPromptSerializer,
    TTPromptSerializer,
    TTTPromptSerializer,
    TTTTPromptSerializer,
)
from api.models.games import Game
from rest_framework.permissions import IsAuthenticated


PROMPT_TYPES = {
    "I": (IPrompt, IPromptSerializer),
    "IT": (ITPrompt, ITPromptSerializer),
    "TT": (TTPrompt, TTPromptSerializer),
    "TTT": (TTTPrompt, TTTPromptSerializer),
    "TTTT": (TTTTPrompt, TTTTPromptSerializer),
}


class ComoPromptLogic(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, game_slug, format=None):
        logging.info("Getting prompts for " + str(game_slug))
        if not game_slug:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        game = get_object_or_404(Game, slug=game_slug)
        if not game.is_active:
            logging.info("Non-active game requested.")
            return Response(
                {"error": "Game is not active"}, status=status.HTTP_404_NOT_FOUND
            )

        prompt_model, serializer_class = PROMPT_TYPES.get(
            game.prompt_type, (None, None)
        )
        if not prompt_model:
            return Response(
                {"error": "Invalid prompt type"}, status=status.HTTP_400_BAD_REQUEST
            )

        prompts = prompt_model.objects.filter(game=game, done=False).order_by("?")[:5]

        if not prompts.exists():
            game.is_active = False
            game.save(update_fields=["is_active"])
            logging.info("Game set to inactive, because no more prompts available.")
            return Response(
                {"error": "No more prompts available. Game has been set to inactive."},
                status=status.HTTP_404_NOT_FOUND,
            )

        serialized_prompts = serializer_class(prompts, many=True)
        return Response(serialized_prompts.data, status=status.HTTP_200_OK)

import gzip
import json
import logging
from typing import Any, Dict, Optional
from django.http import HttpRequest
from rest_framework.response import Response
from rest_framework.views import APIView
from django.template.defaultfilters import slugify
from rest_framework import status


from api.single_views.games.creation_check import (
    check_json_completness,
    check_prompt_labels,
)
from api.permissions import IsWikimediaUser
from api.models.games import Game
from api.utils import send_matrix_msg
from core.settings import MATRIX_ADMIN_ROOM


class GameCreationLogic(APIView):
    permission_classes = [IsWikimediaUser]

    def post(self, request, format=None):
        logging.info("Parsing game creation request")

        json_data = self._parse_request_data(request)
        logging.info(json_data)
        if json_data is None:
            return Response(
                "Invalid or corrupted data", status=status.HTTP_400_BAD_REQUEST
            )

        logging.info("Checking JSON data")
        check_json_completness(json_data)

        if json_data["action_type"] == "none":
            action_type = None
        else:
            action_type = json_data["action_type"]

        check_prompt_labels(json_data)

        # Create a game instance from JSON data
        try:
            game = Game(
                name=json_data["name"],
                slug=slugify(json_data["name"]),
                short_description=json_data["short_description"],
                long_description=json_data["long_description"],
                prompt_type=json_data["prompt_type"],
                prompt_labels_list=json_data.get("prompt_labels"),
                response_type=json_data["response_type"],
                action_type=action_type,
                is_listed=bool(json_data["is_listed"]),
                confidence_type=json_data["confidence_type"],
                question_with_jokers=json_data["question"],
                user=request.user,
            )
            game.save()

        except Exception as e:
            logging.error(e)
            return Response(
                status=status.HTTP_400_BAD_REQUEST, data="Game creation failed"
            )

        send_matrix_msg(
            MATRIX_ADMIN_ROOM,
            "🎮 New game: "
            + game.name
            + " has been created and needs to be reviewed: 🔗 "
            + game.get_public_url(),
        )

        return Response(
            status=status.HTTP_201_CREATED, data="Game created successfully"
        )

    def _parse_request_data(self, request: HttpRequest) -> Optional[Dict[str, Any]]:
        if request.headers.get("Content-Encoding") == "gzip":
            try:
                return json.loads(gzip.decompress(request.body))
            except gzip.error:
                logging.error("Failed to decompress gzip data")
                return None
        return request.data

import csv
import json
import logging
from rest_framework.response import Response
from rest_framework import status
from api.models.games import Game
from typing import Dict, Any

from core.settings import (
    OPTIONAL_FIELDS_PROMPT,
    OPTIONAL_FIELDS_RESPONSE,
    REQUIRED_FIELDS_PROMPT,
    REQUIRED_FIELDS_RESPONSE,
)


def check_json_completness(json_data: Dict[str, Any]) -> Response:
    required_keys = [
        "name",
        "prompt_type",
        "response_type",
        "short_description",
        "long_description",
        "action_type",
        "question",
        "content",
        "is_listed",
        "confidence_type",
    ]

    # Validate required JSON fields
    if not all(key in json_data for key in required_keys):
        return Response(
            status=status.HTTP_400_BAD_REQUEST,
            data="Missing required JSON fields",
        )

    # Check action_type
    if json_data["action_type"] == "WA":
        if "action_requests" not in json_data:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data="Action WA selected, no action_requests included",
            )
        elif len(json_data["action_requests"]) == 0:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data="Action WA selected, action_requests empty",
            )


def check_csv_completeness(game: Game, reader: csv.DictReader):
    logging.info("Checking CSV data")

    # Check if there are headers in the CSV file
    if not reader.fieldnames:
        logging.error("CSV file is empty or improperly formatted.")
        return Response(
            {"error": "CSV file is empty or improperly formatted."},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # Attempt to read at least one data row to ensure the CSV is not just headers
    try:
        first_row = next(reader)
    except StopIteration:
        logging.error("CSV file contains no data.")
        return Response(
            {"error": "CSV file contains no data."}, status=status.HTTP_400_BAD_REQUEST
        )

    logging.info("Check csv for required fields")

    # Combine required fields for the given prompt and response types
    fields_needed = REQUIRED_FIELDS_PROMPT.get(
        game.prompt_type, []
    ) + REQUIRED_FIELDS_RESPONSE.get(game.response_type, [])

    # Include optional fields marked with a question mark
    optional_fields = OPTIONAL_FIELDS_PROMPT.get(
        game.prompt_type, []
    ) + OPTIONAL_FIELDS_RESPONSE.get(game.response_type, [])
    optional_fields_marked = [f"{field}?" for field in optional_fields]

    # Find any missing fields
    missing_fields = [
        field for field in fields_needed if field not in reader.fieldnames
    ]
    if missing_fields:
        error_message = f"Missing required field(s): {', '.join(missing_fields)}"
        logging.error(error_message)
        return Response({"error": error_message}, status=status.HTTP_400_BAD_REQUEST)

    return Response(
        {"required_fields": fields_needed, "optional_fields": optional_fields_marked},
        status=status.HTTP_200_OK,
    )


def check_prompt_labels(json_data) -> None:
    if json_data.get("prompt_labels"):
        try:
            prompt_labels = json.loads(json_data["prompt_labels"])

            if not all(isinstance(label, str) for label in prompt_labels):
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data="All items in 'prompt_labels' must be strings.",
                )

            if len(prompt_labels) > 5:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data="No more than 5 prompt labels are allowed.",
                )

            if any(len(label) > 20 for label in prompt_labels):
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data="Each label must not exceed 20 characters.",
                )
            return json_data["prompt_labels"]

        except json.JSONDecodeError:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data="Failed to decode 'prompt_labels' as JSON.",
            )
    return None

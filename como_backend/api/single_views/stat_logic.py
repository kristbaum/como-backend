import logging
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import Count
from django.shortcuts import get_object_or_404

from api.models.games import Game
from api.models.prompts import ComoPrompt
from api.models.responses import ComoResponse
from api.models.users import ComoUser
from core.settings import CACHE_TTL, REDIS_KEY_PREFIX
from django.core.cache import cache
from django.utils import timezone
from django.db.models.functions import TruncDate


class StatLogic(APIView):

    def get(self, request):
        slug = request.query_params.get("game")
        user_id = request.query_params.get("user")
        user = request.user

        logging.info(f"Getting stats for game: {slug} and user: {user_id}")

        if not slug:
            # Global stats
            return self._global_stats(user)

        # Load game or return 404 if not found
        game = get_object_or_404(Game, slug=slug)

        if user_id is None:
            # Game level stats
            return self._game_stats(game)

        # User level stats
        user = get_object_or_404(ComoUser, id=user_id)
        return self._user_stats(game, user)

    def _game_stats(self, game):
        leaderboard = self._create_leaderboard(game)
        return Response(
            {
                "done_prompts": ComoPrompt.objects.filter(game=game, done=True).count(),
                "total_prompts": ComoPrompt.objects.filter(game=game).count(),
                "leaderboard": leaderboard,
            }
        )

    def _user_stats(self, game, user):
        done_prompts = ComoResponse.objects.filter(prompt__game=game, user=user).count()
        total_prompts = ComoPrompt.objects.filter(game=game).count()

        return Response({"done_prompts": done_prompts, "total_prompts": total_prompts})

    def _create_leaderboard(self, game):
        query = (
            ComoResponse.objects.filter(skipped=False, prompt__game=game)
            .values("user__username", "user__is_wikimedia_user", "user__id")
            .annotate(total_responses=Count("id"))
            .order_by("-total_responses")[:10]
        )

        return [
            {
                "username": entry["user__username"],
                "profile_link": (
                    "https://meta.wikimedia.org/wiki/User:" + entry["user__username"]
                    if entry["user__is_wikimedia_user"]
                    else None
                ),
                "responses_count": entry["total_responses"],
            }
            for entry in query
        ]

    def _global_stats(self, user):
        # Cache keys
        done_prompts_cache_key = f"{REDIS_KEY_PREFIX}_done_prompts_count"
        contributions_cache_key = f"{REDIS_KEY_PREFIX}_global_contributions_count"

        # Try to get values from cache first
        done_prompts = cache.get(done_prompts_cache_key)
        global_contributions = cache.get(contributions_cache_key)

        # If cache is empty, calculate and cache values
        if done_prompts is None:
            done_prompts = ComoPrompt.objects.filter(done=True).count()
            cache.set(done_prompts_cache_key, done_prompts, timeout=CACHE_TTL)

        if global_contributions is None:
            global_contributions = ComoResponse.objects.count()
            cache.set(contributions_cache_key, global_contributions, timeout=CACHE_TTL)

        # Only calculate user-specific stats if the user is authenticated
        if user.is_authenticated:
            your_prompts_cache_key = f"{REDIS_KEY_PREFIX}_your_prompts_count"
            streak_cache_key = f"{REDIS_KEY_PREFIX}_user_streak_{user.id}"

            your_prompts = cache.get(your_prompts_cache_key)
            streak_data = cache.get(streak_cache_key)

            if your_prompts is None:
                your_prompts = ComoResponse.objects.filter(
                    user=user, skipped=False
                ).count()
                cache.set(your_prompts_cache_key, your_prompts, timeout=CACHE_TTL)

            if streak_data is None:
                streak_data = self._calculate_streak(user)
                cache.set(streak_cache_key, streak_data, timeout=CACHE_TTL)

            return Response(
                {
                    "done_prompts": done_prompts,
                    "your_prompts": your_prompts,
                    "streak": streak_data["streak"],
                    "missing_rounds_today": streak_data["missing_rounds_today"],
                }
            )

        # For anonymous users, return only global stats
        return Response(
            {
                "done_prompts": done_prompts,
                "global_contributions": global_contributions,
            }
        )

    def _calculate_streak(self, user):
        today = timezone.now().date()

        # Group responses by date (using `created_at`) and count them per day
        rounds_per_day = (
            ComoResponse.objects.filter(user=user, skipped=False)
            .annotate(date=TruncDate("created_at"))  # Group by date
            .values("date")
            .annotate(rounds=Count("id"))
            .order_by("-date")  # Order by latest date
        )

        streak = 0
        consecutive_days = 0
        previous_day = None
        missing_rounds_today = 10

        for entry in rounds_per_day:
            rounds = entry["rounds"]
            day = entry["date"]

            if day == today:
                # Calculate how many rounds are missing for today
                missing_rounds_today = max(0, 10 - rounds)

            # Check if user played at least 10 rounds on that day
            if rounds >= 10:
                if previous_day is None:
                    previous_day = day
                    consecutive_days = 1
                elif (previous_day - day).days == 1:  # Check if the day is consecutive
                    consecutive_days += 1
                    previous_day = day
                else:
                    break  # If the days are not consecutive, the streak ends
                streak = consecutive_days
            else:
                break  # If the user didn't play enough rounds, the streak ends

        return {"streak": streak, "missing_rounds_today": missing_rounds_today}

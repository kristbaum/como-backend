import json
import logging
import requests
from rest_framework.exceptions import NotAuthenticated
from api.utils import replace_variables
from urllib.parse import urlparse, urlunparse

from api.models.actions import ActionRequest
from api.models.responses import TextResponse, YesNoResponse
from core.views import get_access_token


def evaluate_action(game, como_prompt, como_response, user):
    logging.info("Evaluating action")
    for como_request in ActionRequest.objects.filter(prompt=como_prompt):
        logging.info(
            "executing: "
            + str(como_request.request_url)
            + " with body: "
            + str(como_request.request_body)
        )

        if game.action_type == "WA":
            headers = build_headers(user, "application/json")
            execute_rest_api_action(como_request, como_response, headers)
        elif game.action_type == "AA":
            headers = build_headers(user, "application/x-www-form-urlencoded")
            execute_mediawiki_action_api(como_request, como_response, headers)
        else:
            logging.error("Action type not implemented yet")


def execute_rest_api_action(como_request, como_response, headers):
    if isinstance(como_response, YesNoResponse):
        if como_response.answer_is_yes:
            execute_request(
                como_request.request_method,
                como_request.request_url,
                como_request.request_body,
                headers,
            )
        else:
            logging.info("YN 'No' action triggered, none done")
    elif isinstance(como_response, TextResponse):
        replaced_body = replace_variables(
            como_request.request_body, {"response_text": como_response.text}
        )
        execute_request(
            como_request.request_method,
            como_request.request_url,
            replaced_body,
            headers,
        )
    else:
        logging.error("Response type not implemented yet")


def execute_mediawiki_action_api(como_request, como_response, headers):
    csrf_token = fetch_csrf_token(headers, como_request.request_url)
    if not csrf_token:
        logging.error("Could not obtain CSRF token")
        return

    action_request_body = replace_variables(
        como_request.request_body,
        {"response_text": como_response.text},
    )
    logging.info(f"Processed Action request body: {action_request_body}")
    action_request_json = json.loads(action_request_body)
    logging.info(f"Action request JSON: {action_request_json}")
    action_request_json["token"] = csrf_token
    execute_request(
        como_request.request_method,
        como_request.request_url,
        action_request_json,
        headers,
    )


def fetch_csrf_token(headers, request_url):
    """Fetch a CSRF token from the MediaWiki API using the base URL extracted from request_url"""

    # Parse the URL and rebuild it to stop at 'api.php'
    parsed_url = urlparse(request_url)

    # Automatically extract base URL from request URL
    api_base_url = urlunparse(
        parsed_url._replace(path=parsed_url.path.split("api.php")[0] + "api.php")
    )

    params = {"action": "query", "meta": "tokens", "format": "json", "assert": "user"}

    response = requests.get(api_base_url, headers=headers, params=params)
    if response.status_code == 200 and "query" in response.json():
        logging.info("Fetched CSRF token successfully")
        logging.info(f"Response: {response.json()}")
        # Evaluate success success:1 ["query"]["tokens"]["csrftoken"]
        return response.json()["query"]["tokens"]["csrftoken"]
    else:
        logging.error(
            f"Failed to fetch CSRF token. Status code: {response.status_code} {response.text}"
        )
        return None


def execute_request(request_method, request_url, request_body, headers):
    if request_method == "POST":
        response = requests.post(request_url, data=request_body, headers=headers)
    elif request_method == "PATCH":
        response = requests.patch(request_url, data=request_body, headers=headers)
    elif request_method == "PUT":
        response = requests.put(request_url, data=request_body, headers=headers)

    log_response(response)


def log_response(response):
    if response.status_code == 201:
        # Success return code for Wikibase REST Api
        logging.info("Action triggered successfully")
    else:
        if response.status_code == 200:
            response_json = response.json()
            if "success" in response_json and response_json["success"] == 1:
                logging.info("Action triggered successfully")
                return
        logging.error(
            f"Failed to trigger action. Status code: {response.status_code} {response.text}"
        )
        if response.status_code == 403:
            raise NotAuthenticated(
                "Failed to trigger action, probably out of date OAUTH"
            )


def build_headers(user, content_type):
    access_token = get_access_token(user)
    if not access_token:
        logging.warning("No access token found")
        return {"Content-Type": content_type, "Accept": "application/json"}
    else:
        return {
            "Content-Type": content_type,
            "Accept": "application/json",
            "Authorization": f"Bearer {access_token}",
        }

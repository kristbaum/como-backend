import csv
import logging
from io import StringIO, BytesIO
import zipfile
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from api.models.games import Game
from api.models.prompts import IPrompt, ITPrompt, TTPrompt, TTTPrompt, TTTTPrompt
from api.models.responses import SingleChoiceResponse, TextResponse, YesNoResponse


class ComoDownloadLogic(APIView):
    def download(self, game):
        logging.info(f"Starting CSV download for game: {game}")

        in_memory = BytesIO()
        with zipfile.ZipFile(in_memory, "w", zipfile.ZIP_DEFLATED) as archive:
            csv_buffer = StringIO()
            writer = csv.writer(csv_buffer)

            if hasattr(self, f"handle_{game.prompt_type}_{game.response_type}"):
                handler = getattr(
                    self, f"handle_{game.prompt_type}_{game.response_type}"
                )
                handler(game, writer)
            else:
                logging.error("Unsupported prompt/response combination")
                return Response(
                    {"error": "Unsupported prompt or response type"},
                    status=status.HTTP_501_NOT_IMPLEMENTED,
                )

            archive.writestr(f"{game.slug}_all.csv", csv_buffer.getvalue())

        in_memory.seek(0)
        response = HttpResponse(in_memory, content_type="application/zip")
        response["Content-Disposition"] = f'attachment; filename="{game.slug}_all.zip"'
        return response

    def handle_I_YN(self, game, writer):
        prompts = IPrompt.objects.filter(game=game)
        writer.writerow(["custom_id", "image1", "yns", "done"])
        for prompt in prompts:
            yns = YesNoResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.image1,
                    yns.answer_is_yes if yns else None,
                    prompt.done,
                ]
            )

    def handle_IT_YN(self, game, writer):
        prompts = ITPrompt.objects.filter(game=game)
        writer.writerow(["custom_id", "image1", "text1", "link1", "yns", "done"])
        for prompt in prompts:
            yns = YesNoResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.image1,
                    prompt.text1,
                    prompt.link1,
                    yns.answer_is_yes if yns else None,
                    prompt.done,
                ]
            )

    def handle_TT_YN(self, game, writer):
        prompts = TTPrompt.objects.filter(game=game)
        writer.writerow(
            ["custom_id", "text1", "link1", "text2", "link2", "yns", "done"]
        )
        for prompt in prompts:
            yns = YesNoResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.text1,
                    prompt.link1,
                    prompt.text2,
                    prompt.link2,
                    yns.answer_is_yes if yns else None,
                    prompt.done,
                ]
            )

    def handle_TTT_YN(self, game, writer):
        prompts = TTTPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "text1",
                "link1",
                "text2",
                "link2",
                "text3",
                "link3",
                "yns",
                "done",
            ]
        )
        for prompt in prompts:
            yns = YesNoResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.text1,
                    prompt.link1,
                    prompt.text2,
                    prompt.link2,
                    prompt.text3,
                    prompt.link3,
                    yns.answer_is_yes if yns else None,
                    prompt.done,
                ]
            )

    def handle_TTTT_YN(self, game, writer):
        prompts = TTTTPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "text1",
                "link1",
                "text2",
                "link2",
                "text3",
                "link3",
                "text4",
                "link4",
                "yns",
                "done",
            ]
        )
        for prompt in prompts:
            yns = YesNoResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.text1,
                    prompt.link1,
                    prompt.text2,
                    prompt.link2,
                    prompt.text3,
                    prompt.link3,
                    prompt.text4,
                    prompt.link4,
                    yns.answer_is_yes if yns else None,
                    prompt.done,
                ]
            )

    def handle_I_ET(self, game, writer):
        prompts = IPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "image1",
                "final_text",
                "same_as_suggestion",
                "done",
            ]
        )
        for prompt in prompts:
            te = TextResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.image1,
                    te.text if te else None,
                    te.same_as_prompt if te else None,
                    prompt.done,
                ]
            )

    def handle_IT_ET(self, game, writer):
        prompts = ITPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "image1",
                "text1",
                "link1",
                "final_text",
                "same_as_suggestion",
                "done",
            ]
        )
        for prompt in prompts:
            te = TextResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.image1,
                    prompt.text1,
                    prompt.link1,
                    te.text if te else None,
                    te.same_as_prompt if te else None,
                    prompt.done,
                ]
            )

    def handle_TT_ET(self, game, writer):
        prompts = TTPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "text1",
                "link1",
                "text2",
                "link2",
                "final_text",
                "same_as_suggestion",
                "done",
            ]
        )
        for prompt in prompts:
            te = TextResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.text1,
                    prompt.link1,
                    prompt.text2,
                    prompt.link2,
                    te.text if te else None,
                    te.same_as_prompt if te else None,
                    prompt.done,
                ]
            )

    def handle_TTT_ET(self, game, writer):
        prompts = TTTPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "text1",
                "link1",
                "text2",
                "link2",
                "text3",
                "link3",
                "final_text",
                "same_as_suggestion",
                "done",
            ]
        )
        for prompt in prompts:
            te = TextResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.text1,
                    prompt.link1,
                    prompt.text2,
                    prompt.link2,
                    prompt.text3,
                    prompt.link3,
                    te.text if te else None,
                    te.same_as_prompt if te else None,
                    prompt.done,
                ]
            )

    def handle_TTTT_ET(self, game, writer):
        prompts = TTTTPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "text1",
                "link1",
                "text2",
                "link2",
                "text3",
                "link3",
                "text4",
                "link4",
                "final_text",
                "same_as_suggestion",
                "done",
            ]
        )
        for prompt in prompts:
            te = TextResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.text1,
                    prompt.link1,
                    prompt.text2,
                    prompt.link2,
                    prompt.text3,
                    prompt.link3,
                    prompt.text4,
                    prompt.link4,
                    te.text if te else None,
                    te.same_as_prompt if te else None,
                    prompt.done,
                ]
            )

    def handle_I_SC(self, game, writer):
        prompts = IPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "image1",
                "selected_choice",
                "done",
            ]
        )
        for prompt in prompts:
            sc = SingleChoiceResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.image1,
                    sc.selected if sc else None,
                    prompt.done,
                ]
            )

    def handle_IT_SC(self, game, writer):
        prompts = ITPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "image1",
                "selected_choice",
                "done",
            ]
        )
        for prompt in prompts:
            sc = SingleChoiceResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.image1,
                    sc.selected if sc else None,
                    prompt.done,
                ]
            )

    def handle_TT_SC(self, game, writer):
        prompts = TTPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "text1",
                "link1",
                "text2",
                "link2",
                "selected_choice",
                "done",
            ]
        )
        for prompt in prompts:
            sc = SingleChoiceResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.text1,
                    prompt.link1,
                    prompt.text2,
                    prompt.link2,
                    sc.selected if sc else None,
                    prompt.done,
                ]
            )

    def handle_TTT_SC(self, game, writer):
        prompts = TTTPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "text1",
                "link1",
                "text2",
                "link2",
                "text3",
                "link3",
                "selected_choice",
                "done",
            ]
        )
        for prompt in prompts:
            sc = SingleChoiceResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.text1,
                    prompt.link1,
                    prompt.text2,
                    prompt.link2,
                    prompt.text3,
                    prompt.link3,
                    sc.selected if sc else None,
                    prompt.done,
                ]
            )

    def handle_TTTT_SC(self, game, writer):
        prompts = TTTTPrompt.objects.filter(game=game)
        writer.writerow(
            [
                "custom_id",
                "text1",
                "link1",
                "text2",
                "link2",
                "text3",
                "link3",
                "text4",
                "link4",
                "selected_choice",
                "done",
            ]
        )
        for prompt in prompts:
            sc = SingleChoiceResponse.objects.filter(prompt=prompt).first()
            writer.writerow(
                [
                    prompt.custom_id,
                    prompt.text1,
                    prompt.link1,
                    prompt.text2,
                    prompt.link2,
                    prompt.text3,
                    prompt.link3,
                    prompt.text4,
                    prompt.link4,
                    sc.selected if sc else None,
                    prompt.done,
                ]
            )

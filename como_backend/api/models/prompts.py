from django.db import models
from api.models.games import Game

class ComoPrompt(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    question = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    done_at = models.DateTimeField(null=True)
    done = models.BooleanField(default=False)
    skip_count = models.IntegerField(default=0)
    custom_id = models.CharField(max_length=500, null=True)


class ITPrompt(ComoPrompt):
    image1 = models.URLField(max_length=1000)
    image1_filename_url = models.URLField(max_length=1000)
    text1 = models.TextField()
    link1 = models.URLField(null=True, max_length=1000)


class IPrompt(ComoPrompt):
    image1 = models.URLField(max_length=1000)
    image1_filename_url = models.URLField(max_length=1000)


class TTPrompt(ComoPrompt):
    text1 = models.TextField()
    link1 = models.URLField(null=True, max_length=1000)
    text2 = models.TextField()
    link2 = models.URLField(null=True, max_length=1000)


class TTTPrompt(ComoPrompt):
    text1 = models.TextField()
    link1 = models.URLField(null=True, max_length=1000)
    text2 = models.TextField()
    link2 = models.URLField(null=True, max_length=1000)
    text3 = models.TextField()
    link3 = models.URLField(null=True, max_length=1000)


class TTTTPrompt(ComoPrompt):
    text1 = models.TextField()
    link1 = models.URLField(null=True, max_length=1000)
    text2 = models.TextField()
    link2 = models.URLField(null=True, max_length=1000)
    text3 = models.TextField()
    link3 = models.URLField(null=True, max_length=1000)
    text4 = models.TextField()
    link4 = models.URLField(null=True, max_length=1000)

from django.db import models
from api.models.prompts import ComoPrompt
from api.models.games import Game


class ActionRequestsWithPlaceholders(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    request_method = models.CharField(
        max_length=5, choices=[("PUT", "PUT"), ("PATCH", "PATCH"), ("POST", "POST")]
    )
    request_url = models.URLField(max_length=1000)
    request_body = models.CharField(max_length=1000)


class ActionRequest(models.Model):
    prompt = models.ForeignKey(ComoPrompt, on_delete=models.CASCADE)
    request_method = models.CharField(
        max_length=5, choices=[("PUT", "PUT"), ("PATCH", "PATCH"), ("POST", "POST")]
    )
    request_url = models.URLField(max_length=1000)
    request_body = models.CharField(max_length=1000)
    # TODO: Tag the action on which to trigger in case of YNS

from django.db import models
from api.models import ComoUser
from core.settings import (
    FRONTEND_URL,
    OPTIONAL_FIELDS_PROMPT,
    OPTIONAL_FIELDS_RESPONSE,
    REQUIRED_FIELDS_PROMPT,
    REQUIRED_FIELDS_RESPONSE,
)


class Game(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    short_description = models.CharField(max_length=100)
    long_description = models.TextField(max_length=1500)

    # Main language of the game
    language = models.CharField(max_length=2, default="en")

    # Public or private
    is_listed = models.BooleanField(default=True)
    # For new games, they must be activated by an admin, finished games are archived
    is_active = models.BooleanField(default=False)

    # For archiving the unprocessed sting
    question_with_jokers = models.CharField(max_length=200)

    # Creator of this game
    user = models.ForeignKey(ComoUser, on_delete=models.PROTECT)

    CONFIDENCE_TYPE_CHOICES = [
        ("S", "Single Confirmation"),
        ("L", "Single Confirmation and logged in"),
        ("D", "Double Confirmations"),
        ("E", "Double Confirmation and logged in"),
    ]
    # Models the trust level of results
    confidence_type = models.CharField(
        max_length=1,
        choices=CONFIDENCE_TYPE_CHOICES,
        default="S",
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    PROMPT_TYPES = [
        ("I", "Image"),
        ("T", "One Text"),
        ("TT", "Two Texts"),
        ("TTT", "Three Texts"),
        ("IT", "Image and Text"),
    ]

    prompt_type = models.CharField(
        max_length=8,
        choices=PROMPT_TYPES,
    )
    prompt_labels_list = models.CharField(max_length=300, null=True)

    RESPONSE_TYPES = [
        ("SC", "Single choice"),
        ("ET", "Editable text"),
        ("YN", "Yes/No"),
    ]

    response_type = models.CharField(
        max_length=2,
        choices=RESPONSE_TYPES,
    )

    ACTION_TYPES = [
        ("QS", "Quickstatement"),
        ("WA", "Wikibase REST API"),
        ("AA", "Action API"),
    ]
    action_type = models.CharField(
        max_length=2,
        choices=ACTION_TYPES,
        null=True,
    )

    def __str__(self):
        return str(self.name)

    def get_public_url(self):
        return FRONTEND_URL + "/#/game/" + self.slug

    def get_suggested_csv_headers(self):
        required_fields = REQUIRED_FIELDS_PROMPT.get(
            self.prompt_type, []
        ) + REQUIRED_FIELDS_RESPONSE.get(self.response_type, [])

        optional_fields = OPTIONAL_FIELDS_PROMPT.get(
            self.prompt_type, []
        ) + OPTIONAL_FIELDS_RESPONSE.get(self.response_type, [])

        # Mark optional fields with a question mark
        optional_fields_marked = [f"{field}?" for field in optional_fields]

        allowed_fields = required_fields + optional_fields_marked
        return allowed_fields

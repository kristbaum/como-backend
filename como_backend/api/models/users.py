from django.contrib.auth.models import AbstractUser
from django.db import models


class ComoUser(AbstractUser):
    """
    Represents a user in the Como application.

    Attributes:
        language (str): The user's preferred language, represented as a two-letter code.
        is_wikimedia_user (bool): Indicates whether the user is a Wikimedia user or not.
    """

    language = models.CharField(max_length=2, default="en")
    is_wikimedia_user = models.BooleanField(default=False)

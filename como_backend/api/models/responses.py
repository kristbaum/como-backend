from django.db import models

from api.models.prompts import ComoPrompt
from api.models import ComoUser


class ResponseOption(models.Model):
    prompt = models.ForeignKey(
        ComoPrompt, related_name="response_options", on_delete=models.CASCADE
    )
    sc_id = models.IntegerField(null=True)
    unique_together = ["prompt", "user"]
    text = models.CharField(max_length=200)
    link = models.URLField(null=True, max_length=1000)


class ComoResponse(models.Model):
    prompt = models.ForeignKey(ComoPrompt, on_delete=models.CASCADE)
    user = models.ForeignKey(ComoUser, on_delete=models.CASCADE)
    unique_together = ["prompt", "user"]
    created_at = models.DateTimeField(auto_now_add=True)
    skipped = models.BooleanField()


class YesNoResponse(ComoResponse):
    answer_is_yes = models.BooleanField(null=True)


class TextResponse(ComoResponse):
    text = models.TextField(null=True)
    same_as_prompt = models.BooleanField(null=True)


class SingleChoiceResponse(ComoResponse):
    selected = models.ForeignKey(ResponseOption, on_delete=models.CASCADE)

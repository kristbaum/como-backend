from django.urls import include, path
from rest_framework.routers import DefaultRouter
from api.single_views.prompts.creation_logic import PromptCreationLogic
from api.single_views.prompts.prompt_logic import ComoPromptLogic
from api.single_views.prompts.response_logic import ComoResponseLogic
from api.views import (
    GameViewSet,
    StatView,
)

router = DefaultRouter()
router.register(r"games", GameViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path(
        "games/<slug:game_slug>/create-prompts/",
        PromptCreationLogic.as_view(),
        name="create_prompts",
    ),
    path(
        "games/<slug:game_slug>/prompts/", ComoPromptLogic.as_view(), name="get_prompts"
    ),
    path(
        "games/<slug:game_slug>/prompts/<int:prompt_id>/responses/",
        ComoResponseLogic.as_view(),
        name="create_response",
    ),
    path("stats/", StatView.as_view(), name="stats"),
]

"""
URL configuration for como_backend project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import (
    SpectacularSwaggerView,
    SpectacularAPIView,
)
from core import settings
from core.views import MediaWikiLogin, social_login_success, test_oauth
from allauth.socialaccount.providers.mediawiki import views as mediawiki_views

urlpatterns = [
    path("accounts/", include("allauth.urls")),
    path("api/", include("api.urls")),
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "api/schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path("auth/", include("dj_rest_auth.urls")),
    path("auth/registration/", include("dj_rest_auth.registration.urls")),
    path("auth/mediawiki/", MediaWikiLogin.as_view(), name="mw_login"),
    path("auth/mediawiki/url/", mediawiki_views.oauth2_login),
    path("auth/test_oauth/", test_oauth, name="test_oauth"),
    path("login_success/", social_login_success, name="social_login_success"),
]

if settings.DEBUG_MODE:
    urlpatterns += [
        path("admin/", admin.site.urls),
    ]

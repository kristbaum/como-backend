import logging
import datetime
import requests
from allauth.socialaccount.providers.mediawiki.views import MediaWikiOAuth2Adapter
from dj_rest_auth.registration.views import SocialLoginView
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from django.shortcuts import redirect
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from allauth.socialaccount.models import SocialToken, SocialApp
from django.utils.timezone import now
from typing import Optional
from core.settings import (
    ACCOUNT_DEFAULT_HTTP_PROTOCOL,
    BASE_URL,
    FRONTEND_URL,
    ADMIN_LIST,
)


class MediaWikiLogin(SocialLoginView):
    adapter_class = MediaWikiOAuth2Adapter
    callback_url = "https://" + BASE_URL
    client_class = OAuth2Client


def social_login_success(request):
    user = request.user
    token, created = Token.objects.get_or_create(user=user)
    frontend_url = ACCOUNT_DEFAULT_HTTP_PROTOCOL + "://" + FRONTEND_URL

    # Log relevant information
    logging.info(f"User: {user.username} signed in using WikiMedia OAuth2")
    user.is_wikimedia_user = True
    if user.username in ADMIN_LIST:
        user.is_staff = True
        logging.info(f"User: {user.username} is an admin")
    user.save()

    return redirect(f"{frontend_url}#/?token={token.key}&username={user.username}")


@api_view(("GET",))
def test_oauth(request):
    logging.info("Testing OAuth2")

    token = get_access_token(request.user)

    if not token:
        logging.error("No token found for the user.")
        return Response(
            {"error": "No token found"}, status=status.HTTP_401_UNAUTHORIZED
        )

    profile_url = "https://meta.wikimedia.org/w/rest.php/oauth2/resource/profile"

    resp = requests.get(
        profile_url,
        headers={"Authorization": f"Bearer {token}"},
    )

    if resp.status_code == 401:
        logging.info("Token expired or invalid, refreshing token...")
        token = refresh_access_token(request.user)
        if not token:
            logging.error("Failed to refresh token.")
            return Response(
                {"error": "Failed to refresh token"},
                status=status.HTTP_401_UNAUTHORIZED,
            )

        resp = requests.get(
            profile_url,
            headers={"Authorization": f"Bearer {token}"},
        )

    resp.raise_for_status()
    logging.info("Token still valid")
    return Response(status=status.HTTP_200_OK)


def get_access_token(user) -> Optional[str]:
    logging.info("Getting access token for user: %s", user.username)
    try:
        token = SocialToken.objects.get(
            account__user=user, account__provider="mediawiki"
        )
        logging.info("Access token expiration time: %s", token.expires_at)
        if token.expires_at and token.expires_at < now():
            logging.info("Access token expired, refreshing...")
            return refresh_access_token(user)
        return token.token
    except SocialToken.DoesNotExist:
        logging.error("SocialToken does not exist for the user: %s", user.username)
        return None


def refresh_access_token(user) -> Optional[str]:
    try:
        token = SocialToken.objects.get(
            account__user=user, account__provider="mediawiki"
        )
        app = SocialApp.objects.get(provider="mediawiki")

        payload = {
            "grant_type": "refresh_token",
            "client_id": app.client_id,
            "client_secret": app.secret,
            "refresh_token": token.token_secret,
        }

        response = requests.post(
            "https://meta.wikimedia.org/w/rest.php/oauth2/access_token", data=payload
        )
        response_data = response.json()

        if response.status_code == 200 and "access_token" in response_data:
            token.token = response_data["access_token"]
            token.expires_at = now() + datetime.timedelta(
                seconds=response_data.get("expires_in", 3600)
            )  # Ensure 'expires_in' exists
            token.save()
            logging.info("Token refreshed successfully for user: %s", user.username)
            return token.token
        else:
            logging.error(
                "Failed to refresh token for user %s: %s", user.username, response_data
            )
            return None
    except SocialToken.DoesNotExist:
        logging.error("SocialToken does not exist for the user: %s", user.username)
        return None
    except Exception as e:
        logging.error(
            "An error occurred while refreshing the token for user %s: %s",
            user.username,
            str(e),
        )
        return None
